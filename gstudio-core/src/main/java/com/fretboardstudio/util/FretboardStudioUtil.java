package com.fretboardstudio.util;

import com.fretboardstudio.Note;

import java.util.Arrays;

/**
 * Miscellaneous utility methods and constants for working with notes on a
 * fretboard. NOTE: All values are zero based when dealing with not intervals.
 * 
 * @author John Brainard
 */
public class FretboardStudioUtil {
	public static final String[] CHROMATIC_SCALE = new String[] {
		"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"
	};
	
	public static final String[] CHROMATIC_SCALE_FLAT = new String[] {
		"C","Db","D","Eb","E","F","Gb","G","Ab","A","Bb","B"
	};

	/**
	 * Note names in standard tuning.
	 */
	public static final String[] S_STANDARD_TUNING = new String[] {
		"E", "A", "D", "G", "B", "E"
	};
	
	public static final Note[] STANDARD_TUNING = new Note[] {
		Note.E, Note.A, Note.D, Note.G, Note.B, Note.E
	};

	/**
	 * Returns the name of the note at the position on the string. This uses
	 * the standard tuning (E, A, D, G, B, E) for reference.
	 * 
	 * @param string Number of the string (zero based) starting at low E.
	 * @param fret Number of the fret (zero being the nut).
	 * @return
	 */
	public static Note getNoteAtFretOnString(int string, int fret) {
		return getNoteAtFretOnString(string, fret, STANDARD_TUNING);
	}

	/**
	 * Returns the name of the note at the position on the string.
	 * 
	 * @param string Number of the string (zero based).
	 * @param fret Number of the fret (zero being the nut).
	 * @param tuning Array of notes starting on the low string that make up the tuning.
	 * @return
	 */
	public static Note getNoteAtFretOnString(int string, int fret, Note[] tuning) {
		Note nutNote = tuning[string];

		return nutNote.getNoteAt(fret);
	}

	/**
	 * Convert an array of notes to intervals based on the root note. Useful
	 * for converting a known chord to its intervals.
	 * 
	 * @param rootNote
	 * @param noteNames
	 * @return Array containing distances from the root note.
	 */
	public static int[] convertNotesToIntervals(String rootNote, String[] noteNames) {
		Note root = Note.getNote(rootNote);
		Note[] notes = new Note[noteNames.length];

		for(int i = 0; i < noteNames.length; i++) {
			notes[i] = Note.getNote(noteNames[i]);
		}

		return convertNotesToIntervals(root, notes);
	}

	/**
	 * Convert an array of notes to intervals based on the root note. Useful
	 * for converting a known chord to its intervals.

	 * @param rootNote
	 * @param notes
	 * @return
	 */
	public static int[] convertNotesToIntervals(Note rootNote, Note[] notes) {
		int[] intervals = new int[notes.length];

		for(int i = 0; i < notes.length; i++) {
			intervals[i] = rootNote.getDistance(notes[i]);
		}

		return intervals;
	}

	/**
	 * Return a new mode of the scale based off of the offset value
	 * 
	 * @param intervals
	 * @param newRoot
	 * @return
	 */
	public static int[] translateScaleMode(int[] intervals, int offset) {
		int[] newIntervals = shiftScaleLeft(intervals, offset);
		int distance = newIntervals[0] - intervals[0];

		for(int i = 0; i < intervals.length; i++) {
			newIntervals[i] = (newIntervals[i] - distance);
			
			if(newIntervals[i] < 0)
				newIntervals[i] = 12 + newIntervals[i];
		}

		return newIntervals;
	}

	/**
	 * Shift intervals left, wrapping the the items at the beginning to the
	 * end. This is used by translateScaleMode to create a new scale mode.
	 *  
	 * @param scale Array of intervals to shift.
	 * @param amount Number of positions to shift.
	 * @return
	 */
	public static int[] shiftScaleLeft(int[] scale, int amount) {
		int[] shifted = new int[scale.length];
		
		for(int i = 0; i < scale.length; i++ ) {
			shifted[i] = scale[(amount + i) % scale.length];
		}

		return shifted;
	}

	public static boolean isSubsetOf(int[] array, int[] set) {
		int[] sorted = Arrays.copyOf(set, set.length);
		Arrays.sort(sorted);

		boolean found = true;

		for(int i : array) {
			found =  Arrays.binarySearch(sorted, i) >= 0;

			if(!found)
				break;
		}
		
		return found;
	}
}
