package com.fretboardstudio.util;

import java.util.ArrayList;
import java.util.List;

import com.fretboardstudio.Chord;
import com.fretboardstudio.Scale;
import com.fretboardstudio.ScaleChords;

/**
 * Utility class for mapping chords and scale associations.
 *
 * @author John Brainard
 */
public class ChordScaleMapUtil {
	private List<Chord> chords;

	public ChordScaleMapUtil(List<Chord> chords) {
		this.chords = chords;
	}

	public List<ScaleChords> findChords(int[] scaleIntervals) {
		List<ScaleChords> scaleChords = new ArrayList<>();
		for (int i = 0; i < scaleIntervals.length; i++) {
			int[] mode = FretboardStudioUtil.translateScaleMode(scaleIntervals, i);

			List<Chord> chords = new ArrayList<>();

			for (Chord chord : this.chords) {
				if (FretboardStudioUtil.isSubsetOf(chord.getIntervals(), mode))
					chords.add(chord);
			}

			ScaleChords sc = new ScaleChords(chords, scaleIntervals[i]);
			scaleChords.add(sc);
		}

		return scaleChords;
	}
}
