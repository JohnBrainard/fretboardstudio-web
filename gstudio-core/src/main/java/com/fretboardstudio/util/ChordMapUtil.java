package com.fretboardstudio.util;

import com.fretboardstudio.Chord;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class ChordMapUtil {
	private static final Logger log = Logger.getLogger(ChordMapUtil.class.getName());

	private Map<String, Chord> chordMap;

	public ChordMapUtil(List<Chord> chords) {
		this.chordMap = new HashMap<String, Chord>();

		for (Chord chord : chords) {
			String key = getKey(chord.getIntervals());

			chordMap.put(key, chord);
		}
	}

	public Chord findChord(int[] intervals) {
		String key = getKey(intervals);
		Chord chord = chordMap.get(key);

		if (chord != null)
			return chord;

		log.info("Unable to find an exact match for: " + key);
		return chordMap.values().stream()
				.filter(c -> intervalsSubsetOf(c.getIntervals(), intervals))
				.findFirst()
				.orElse(null);
	}

	String getKey(int[] intervals) {
		int[] sorted = Arrays.copyOf(intervals, intervals.length);
		Arrays.sort(sorted);

		String key = "";

		for (int i = 0; i < sorted.length; i++) {
			if (i > 0)
				key += ",";

			key += String.valueOf(sorted[i]);
		}

		return key;
	}

	private static boolean intervalsSubsetOf(int[] intervals, int[] subset) {
		if (intervals == null || intervals.length == 0 || subset == null || subset.length == 0)
			return false;

		outer:
		for (int item : subset) {
			for (int compareTo : intervals) {
				if (item == compareTo)
					continue outer;
			}

			return false;
		}

		return true;
	}
}
