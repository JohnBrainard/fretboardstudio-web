package com.fretboardstudio;

/**
 * 
 * 
 * @author John Brainard
 *
 */
public enum Note {
	A		("A", 9),
	A_SHARP	("A#", "Bb", 10),
	B		("B", 11),
	C		("C", 0),
	C_SHARP	("C#", "Db", 1),
	D		("D", 2),
	D_SHARP ("D#", "Eb", 3),
	E		("E", 4),
	F		("F", 5),
	F_SHARP	("F#", "Gb", 6),
	G		("G", 7),
	G_SHARP	("G#", "Ab", 8),
	MUTED	("X", ".", -1);

	private final String name;
	private final String flatName;
	private final int position;

	Note(String name, int position) {
		this(name, name, position);
	}
	
	Note(String name, String flatName, int position) {
		this.name = name;
		this.flatName = flatName;
		this.position = position;
	}
	
	public String getName() {
		return name;
	}
	
	public String getFlatName() {
		return flatName;
	}
	
	public int getPosition() {
		return position;
	}
	
	public int getDistance(Note note) {
		return (note.position + (12 - position)) % 12; 
	}
	
	public Note getNoteAt(int position) {
		return getNote( (this.position + position) % 12);
	}

	public static Note getNote(int position) {
		switch(position) {
			case -1: return MUTED;
			case 0: return C;
			case 1: return C_SHARP;
			case 2: return D;
			case 3: return D_SHARP;
			case 4: return E;
			case 5: return F;
			case 6: return F_SHARP;
			case 7: return G;
			case 8: return G_SHARP;
			case 9: return A;
			case 10: return A_SHARP;
			case 11: return B;
		}

		throw new IllegalArgumentException("Position " + position + " is invalid. Must be between 0 and 11.");
	}

	public Note[] getNotes(int[] intervals) {
		Note[] notes = new Note[intervals.length];

		for(int i = 0; i < intervals.length; i++) {
			notes[i] = getNoteAt(intervals[i]);
		}

		return notes;
	}

	/**
	 * Attempts to convert the string into a Note instance. <code>getNote</code>
	 * attempts to call <code>Note.valueOf(_name)</code> first.
	 * 
	 * @param _name Name of the note.
	 * @return
	 */
	public static Note getNote(String _name) {
		try {
			return Note.valueOf(_name);
		} catch (IllegalArgumentException ex) {
			String name = _name.toUpperCase();

			if("C#".equals(name) || "DB".equals(name))
				return C_SHARP;
			
			if("D#".equals(name) || "EB".equals(name))
				return D_SHARP;
			
			if("F#".equals(name) || "GB".equals(name))
				return F_SHARP;
			
			if("G#".equals(name) || "AB".equals(name))
				return G_SHARP;
			
			if("A#".equals(name) || "BB".equals(name))
				return A_SHARP;

			throw new IllegalArgumentException("'" + name + "' not recognized as valid note name.");
		}
	}
}
