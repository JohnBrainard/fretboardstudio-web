package com.fretboardstudio

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fretboardstudio.json.NoteJsonSerializer
import java.util.*

data class ChordFingering(
		val id: String?,
		val name: String,
		val chordId: String?,
		@get:JsonSerialize(using = NoteJsonSerializer::class)
		val rootNote: Note,
		val notes: List<ChordNote>,
		val fret: Int) {

	fun getRawNotes(): Array<Note> = notes.map { it.note }.toTypedArray()

	companion object {
		fun build(construct: Builder.() -> Unit): ChordFingering {
			val builder = Builder()
			builder.construct()
			return builder.build()
		}
	}

	class Builder(
			var id: String? = null,
			var name: String? = null,
			var chordId: String? = null,
			var rootNote: Note? = null,
			var notes: MutableList<ChordNote> = ArrayList(),
			var fret: Int? = null) {

		fun id(value: String): Builder {
			id = value
			return this
		}

		fun name(value: String): Builder {
			name = value
			return this
		}

		fun chordId(value: String): Builder {
			chordId = value
			return this
		}

		fun rootNote(value: Note): Builder {
			rootNote = value
			return this
		}

		fun note(construct: ChordNote.Builder.() -> Unit): Builder = note(ChordNote.build(construct))

		fun note(value: ChordNote): Builder {
			notes.add(value)
			return this
		}

		fun fret(value: Int): Builder {
			fret = value
			return this
		}

		fun build(): ChordFingering {
			if (name == null || rootNote == null || fret == null)
				throw IllegalStateException("name, rootNote or fret cannot be null")
			return ChordFingering(id, name!!, chordId, rootNote!!, notes, fret!!)
		}
	}
}