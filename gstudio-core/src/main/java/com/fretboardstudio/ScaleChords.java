package com.fretboardstudio;


import java.util.List;
import java.util.stream.Collectors;

/**
 * Associates chords with scales.
 *
 * @author John Brainard
 */
public class ScaleChords {
	private Long id;

	private Note rootNote;

	private List<String> chordIds;
	private List<Chord> chords;

	private Integer interval;

	public ScaleChords(List<Chord> chords, Integer interval) {
		this.chords = chords;
		this.chordIds = chords.stream()
				.map(chord -> chord.getId())
				.collect(Collectors.toList());
		this.interval = interval;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Note getRootNote() {
		return rootNote;
	}

	public void setRootNote(Note rootNote) {
		this.rootNote = rootNote;
	}

	public List<Chord> getChords() {
		return chords;
	}

	public List<String> getChordIds() {
		return chordIds;
	}

	public void setChordIds(List<String> chordIds) {
		this.chordIds = chordIds;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}
}
