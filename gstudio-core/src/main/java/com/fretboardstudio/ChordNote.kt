package com.fretboardstudio

import com.fasterxml.jackson.databind.annotation.JsonSerialize

class ChordNote(
		@get:JsonSerialize(using = com.fretboardstudio.json.NoteJsonSerializer::class)
		val note: Note,
		val string: Int,
		val fret: Int,
		val finger: Int,
		val optional: Boolean,
		val rootNote: Boolean) {

	companion object {
		fun build(construct: Builder.() -> Unit): ChordNote {
			val builder = Builder()
			builder.construct()
			return builder.build()
		}
	}

	class Builder(
			var note: Note? = null,
			var string: Int = 0,
			var fret: Int = 0,
			var finger: Int = 0,
			var isOptional: Boolean = false,
			var isRootNote: Boolean = false) {


		fun note(value: Note): Builder {
			note = value
			return this
		}

		fun string(value: Int): Builder {
			string = value
			return this
		}

		fun fret(value: Int): Builder {
			fret = value
			return this
		}

		fun finger(value: Int): Builder {
			finger = value
			return this
		}

		fun isOptional(value: Boolean): Builder {
			isOptional = value
			return this
		}

		fun isRootNote(value: Boolean): Builder {
			isRootNote = value
			return this
		}

		fun build(): ChordNote = ChordNote(note!!, string, fret, finger, isOptional, isRootNote)
	}

}