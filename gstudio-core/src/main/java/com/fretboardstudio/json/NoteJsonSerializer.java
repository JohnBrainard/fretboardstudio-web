package com.fretboardstudio.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fretboardstudio.Note;

import java.io.IOException;

/**
 * Created by John on 10/8/15.
 */
public class NoteJsonSerializer extends JsonSerializer<Note> {
	@Override
	public void serialize(Note value, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
		generator.writeStartObject();
		generator.writeStringField("name", value.getName());
		generator.writeStringField("flatName", value.getFlatName());
		generator.writeEndObject();
	}
}
