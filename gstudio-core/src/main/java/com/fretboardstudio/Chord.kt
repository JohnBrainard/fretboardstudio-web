package com.fretboardstudio

import com.fretboardstudio.util.generateSlug
import com.fretboardstudio.util.orDefault
import java.util.*

data class Chord(val id:String?, val name:String, val slug:String, val intervals:IntArray) {

	constructor(name:String, slug:String, intervals:IntArray) : this(null, name, slug, intervals)

	public fun getNotes(root: Note): Array<Note> = root.getNotes(this.intervals)

	class Builder {
		var id:String? = null
		var name:String? = null
		var slug:String? = null
			get() = field.orDefault(name.generateSlug())

		var intervals = ArrayList<Int>()

		fun id(value:String):Builder {
			id = value
			return this
		}

		fun name(value:String):Builder {
			name = value
			return this
		}

		fun slug(value:String):Builder {
			slug = value
			return this
		}

		fun interval(value:Int):Builder {
			intervals.add(value)
			return this
		}

		fun intervals(vararg value:Int):Builder {
			value.forEach { intervals.add(it) }
			return this
		}

		fun build():Chord {
			return Chord(
					id=this.id,
					name=this.name!!,
					slug=this.slug!!,
					intervals=this.intervals.toIntArray())
		}
	}
}
