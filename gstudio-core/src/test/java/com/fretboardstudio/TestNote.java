package com.fretboardstudio;

import junit.framework.TestCase;
import org.junit.Test;

public class TestNote extends TestCase {

	@Test
	public void testGetPosition() {
		// C,Db,D,Eb,E,F,Gb,G,G#,A,Bb,B
		assertEquals(0, Note.C.getDistance(Note.C));
		assertEquals(0, Note.B.getDistance(Note.B));
		assertEquals(11, Note.D_SHARP.getDistance(Note.D));
	}
}
