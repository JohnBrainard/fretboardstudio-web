package com.fretboardstudio.util;

import static com.fretboardstudio.Note.B;
import static com.fretboardstudio.Note.D;
import static com.fretboardstudio.Note.E;
import static org.junit.Assert.*;

import org.junit.Test;

import com.fretboardstudio.Note;

public class TestFretboardStudioUtil {
	@Test
	public void testGetNoteAtFretOnString() {
		Note eOnE = FretboardStudioUtil.getNoteAtFretOnString(0, 0);
		Note dOnA = FretboardStudioUtil.getNoteAtFretOnString(1, 5);
		Note eOnG = FretboardStudioUtil.getNoteAtFretOnString(3, 9);
		Note bOnB = FretboardStudioUtil.getNoteAtFretOnString(4, 12);

		assertEquals(eOnE, E);
		assertEquals(dOnA, D);
		assertEquals(eOnG, E);
		assertEquals(bOnB, B);
	}

	@Test 
	public void testConvertNotesToIntervals() {

		int[] intervals = FretboardStudioUtil.convertNotesToIntervals(
				"C", new String[] { "C", "E", "G"});

		int[] intervals2 = FretboardStudioUtil.convertNotesToIntervals(
				"C", new String[] { "C", "E", "G#", "B" } );

		int[] intervals3 = FretboardStudioUtil.convertNotesToIntervals(
				"B", new String[] { "B", "C#", "D#", "G#", "A" });

		assertArrayEquals(new int[] { 0, 4, 7 }, intervals);
		assertArrayEquals(new int[] { 0, 4, 8, 11 }, intervals2);
		assertArrayEquals(new int[] { 0, 2, 4, 9, 10 }, intervals3);
	}

	@Test
	public void testTranslateScaleMode() {
		int[] scale = new int[] {0, 2, 4, 5, 7, 9, 11};
		int[] mode = FretboardStudioUtil.translateScaleMode(scale, 1);

		assertArrayEquals(new int[] { 0, 2, 3, 5, 7, 9, 10}, mode);

		mode = FretboardStudioUtil.translateScaleMode(scale, 6);
		assertArrayEquals(new int[] { 0, 1, 3, 5, 6, 8, 10}, mode);
	}

	@Test
	public void testShiftScaleLeft() {
		int[] scale = new int[] {0, 2, 4, 5, 7, 9, 11};
		int[] shifted = FretboardStudioUtil.shiftScaleLeft(scale, 1);

		assertArrayEquals(new int[] {2, 4, 5, 7, 9, 11, 0}, shifted);
		
		shifted = FretboardStudioUtil.shiftScaleLeft(scale, 6);
		assertArrayEquals(new int[] {11, 0, 2, 4, 5, 7, 9}, shifted);
		
		shifted = FretboardStudioUtil.shiftScaleLeft(scale, 7);
		assertArrayEquals(scale, shifted);
	}
	
	@Test
	public void testIsSubsetOf() {
		int[] scale = new int[] { 0, 2, 4, 5, 7, 9, 11};
		
		boolean isSubsetOf = FretboardStudioUtil.isSubsetOf(
				new int[] { 0, 4, 7 }, scale);
		assertTrue(isSubsetOf);
		
		isSubsetOf = FretboardStudioUtil.isSubsetOf(
				new int[] { 0, 4, 6, 7 }, scale);
		assertTrue(!isSubsetOf);
	}
}
