package com.fretboardstudio.util;

import java.util.Arrays;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;

import com.fretboardstudio.Chord;

public class TestChordMapUtil extends TestCase {
	private static final Chord[] SAMPLES = new Chord[]{
			new Chord.Builder().name("Major").intervals(0, 4, 7).build(),
			new Chord.Builder().name("Minor").intervals(0, 3, 7).build(),
			new Chord.Builder().name("Maj 7").intervals(0, 4, 7, 11).build()
	};

	@Test
	public void testFindChord() {
		ChordMapUtil cmu = new ChordMapUtil(Arrays.asList(SAMPLES));

		Chord major = cmu.findChord(new int[]{0, 4, 7});
		Chord major2 = cmu.findChord(new int[]{4, 0, 7});
		Chord minor = cmu.findChord(new int[]{0, 3, 7});

		assertNotNull(major);
		assertNotNull(major2);
		assertNotNull(minor);

		assertEquals("Major", major.getName());
		assertEquals("Major", major.getName());
		assertEquals("Minor", minor.getName());
	}

}
