package com.fretboardstudio.util;

import com.fretboardstudio.Chord;
import com.fretboardstudio.Scale;
import com.fretboardstudio.ScaleChords;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class TestChordScaleMapUtil extends TestCase {
	private static final Logger log = Logger.getLogger(TestChordScaleMapUtil.class.getName());

	private List<Chord> chords = new ArrayList<>();

	private final Scale scaleMajor = new Scale.Builder()
			.name("Major")
			.intervals(0, 2, 4, 5, 7, 9, 11)
			.build();

	private final Chord chordMajor = new Chord.Builder()
			.name("Major")
			.intervals(0, 4, 7)
			.build();

	private final Chord chord5 = new Chord.Builder()
			.name("5")
			.intervals(0, 7)
			.build();

	private final Chord chord7 = new Chord.Builder()
			.name("7")
			.intervals(0, 4, 7, 10)
			.build();


	@Override
	protected void setUp() throws Exception {
		super.setUp();

		chords.add(chordMajor);
		chords.add(chord5);
		chords.add(chord7);
	}

	@Test
	public void testFindChords() {
		ChordScaleMapUtil chordScaleMap = new ChordScaleMapUtil(this.chords);

		List<ScaleChords> scaleChords = chordScaleMap.findChords(scaleMajor.getIntervals());

		assertNotNull(scaleChords);
		assertEquals(7, scaleChords.size());

		ScaleChords chords = scaleChords.get(0);
		assertEquals(2, chords.getChords().size());

		chords = scaleChords.get(1);
		assertEquals(1, chords.getChords().size());
	}

}
