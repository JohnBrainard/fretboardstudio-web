package com.fretboardstudio.util

val slugRegex = "[^A-Za-z0-9_-]".toRegex()

/**
 * Generate a slug for use as an identifier from a string such as a title or name
 */
fun String?.generateSlug() = slugRegex.replace(this.orEmpty(), "_").toLowerCase()

/**
 * Return the default value if `this` is null
 */
fun String?.orDefault(default:String) = if (this != null) this else default