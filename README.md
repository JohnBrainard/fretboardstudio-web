FretboardStudio
---------------

**FretboardStudio** is a guitar reference tool which enables easy chord discovery for any selected key. It is a resource I would have loved to have had as I was learning to play and during the period where I set aside more time for playing.

**FretboardStudio** was originally developed to run on Google's AppEngine. After Google deprecated their Master-Slave Datastore, FretboardStudio ceased to function. I have since converted the Maven projects to Gradle projects and replaced the data layer with MySQL and added Spring dependency injection and MVC web framework. Also, much of the new code was written in Kotlin in order to take advantage of its terseness.

## gstudio-dao
The DAO module is responsible for defining the data classes (`Chord`, `Scale`, `ChordFingering`, etc...), the DAO interfaces (`ChordDAO`, `ScaleDAO`, `ChordFingeringDAO`, etc...) and the MySQL implementations of each. It also defines the utility code which loads the raw data for insertion into the database (`ChordFileImporter`, `ScaleFileImporter`, `ChordFingeringFileImporter`).

## gstudio-web
The web module is responsible for the front end and supporting the front end. **FretboardStudio** takes advantage of the [Twitter Bootstrap](http://getbootstrap.com/) library for its UI components and CSS layout framework.

The application Javascript is located in the following folder:

```
./gstudio-web/src/main/resources/static/gstudio-app
```

The most notable javascript classes are:

* [GStudioApp.js](gstudio-web/src/main/resources/static/gstudio-app/GStudioApp.js) encapsulates the core functionality of the FretboardStudio application.
* [Remote.js](gstudio-web/src/main/resources/static/gstudio-app/Remote.js) is responsible for communicating with the server through JSON services.
* [FretboardSVGCanvas.js](gstudio-web/src/main/resources/static/gstudio-app/FretboardSVGCanvas.js) encapsulates the drawing of the main fretboard diagram.
* [Everything else](gstudio-web/src/main/resources/static/gstudio-app/)

## gstudio-tools

Commandline tools for working with the raw data. This code simply sets up a spring `ApplicationContext` and imports POJOs from the other modules and creates a command line interface to the importer tools.

## gstudio-util

A repo for reusable code

## gstudio-domain

*This is old code that will soon be deleted and be replaced with a gstudio-service module which coordinate access to the database and a temporary memory cache such as [Redis](http://redis.io/).*

**gstudio-domain** is responsible for the services to support the front end, including caching frequently used objects in a [memory cache](https://cloud.google.com/appengine/docs/java/memcache/) and JSON serialization. This module also contains the original servlets which acted as the JSON service endpoints for the UI.
