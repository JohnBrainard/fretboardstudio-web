var app, newSheetDialog;

$(function() {
	app = GStudioApp.getInstance();
	newSheetDialog = new NewSheetDialog();

	app.setAppLocation("${cp}");

	$(document).wire(app);
	$("#newSheetDialog").wire(newSheetDialog);

	app.setNewSheetDialog(newSheetDialog);

	app.getProgress().addListener({
		progressComplete : function() {
			$(".spinner").hide();

			scalesLoaded();
		}
	});
});

function scalesLoaded() {
	var scales = app.getScales();
	var templates = app.getTemplates();

	$(".scale-selection").each(function() {
		for(var idx in scales) {
			var scale = scales[idx];

			$(this).append("<option value='" + scale.getName() + "'>" + scale.getName() + "</option>");
		}
	});

	$(".template-selection").each(function() {
		for(var idx in templates) {
			var template = templates[idx];

			$(this).append("<option value='" + template.name + "'>" + template.name + "</option>");
		}
	});
}