function Sheet(data) {
	this.data = data;

	var app = GStudioApp.getInstance();
	this.template = app.getTemplates()[data.template];

	return this;
}

/*****************************************************************************
 * Initialization Routine(s) 
 *****************************************************************************/
Sheet.prototype.initialize = function() {
	var app = GStudioApp.getInstance();
	var scale = app.getScales()[this.data.rootScale];

	if(this.$mainFretboard) {
		var canvas = $("<canvas width='820' height='150'/>").appendTo(this.$mainFretboard);
		var fbc = new FretboardCanvas(canvas.get(0));

		fbc.setNotes(scale.getNotes(this.data.rootNote));
		fbc.draw();

		this.$context.find("[for='mainFretboard']").text(
				this.data.rootNote + ' ' + this.data.rootScale);
	}

	if(this.$title)
		this.$title.text(this.name);
};

/*****************************************************************************
 * Property Getters & Setters 
 *****************************************************************************/
Sheet.prototype.getName = function() { return this.data.name; };
Sheet.prototype.getRootNote = function() { return this.data.rootNote; };
Sheet.prototype.getRootScale = function() { return this.data.rootScale; };
Sheet.prototype.getTemplate = function() { 
	var app = GStudioApp.getInstance();
	return app.getTemplates()[this.data.template];
};


Sheet.prototype.getData = function() { return this.data; };
Sheet.prototype.setData = function(data) { this.data = data; };
