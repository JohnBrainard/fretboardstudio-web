function NewSheetDialog() {
	this.listeners = new Array();
}

NewSheetDialog.prototype.initialize = function() {
	var base = this;
	this.$context.modal({
		backdrop: true,
		keyboard: true,
		show: false
	});
	
	this.$context.bind('shown', function() {
		base.$sheetName.get(0).focus();
	});
};

NewSheetDialog.prototype.show = function() {
	this.$context.modal('show');
};

NewSheetDialog.prototype.addListener = function(listener) {
	this.listeners.push(listener);
};

/*****************************************************************************
 * UI Event Handlers
 *****************************************************************************/
NewSheetDialog.prototype.btnSave_onClick = function() {
	this.$context.modal('hide');

	var base = this;
	var options = {
			name: this.$sheetName.val(),
			template: this.$template.val(),
			rootNote: this.$rootNote.val(),
			rootScale: this.$rootScale.val()
	};

	for(var idx in this.listeners) {
		var listener = this.listeners[idx];

		if(typeof(listener.onNewSheet) === 'function')
			listener.onNewSheet.apply(listener, [base, options]);
	}
};

NewSheetDialog.prototype.btnCancel_onClick = function() {
	this.$context.modal('hide');
};