function Scale(id, name, intervals, type) {
	this.id = id;
	this.intervals = intervals || {};
	this.name = name;
	
	this.type = type === 'undefined' ? 'scale' : 'chord';
}

Scale.CHROMATIC_NOTES = {
	'C': 1,
	'C#': 2, 'Db': 2, 
	'D': 3,
	'D#': 4, 'Eb': 4,
	'E': 5,
	'F': 6,
	'F#': 7, 'Gb' : 7,
	'G': 8,
	'G#': 9, 'Ab': 9,
	'A': 10,
	'A#': 11, 'Bb': 11,
	'B': 12
};

Scale.CHROMATIC_SHARP = {
		1: 'C',
		2: 'C#',
		3: 'D',
		4: 'D#',
		5: 'E',
		6: 'F',
		7: 'F#',
		8: 'G',
		9: 'G#',
		10: 'A',
		11: 'A#',
		12: 'B'
};

Scale.CHROMATIC_FLAT = {
		1: 'C',
		2: 'Db',
		3: 'D',
		4: 'Eb',
		5: 'E',
		6: 'F',
		7: 'Gb',
		8: 'G',
		9: 'Ab',
		10: 'A',
		11: 'Bb',
		12: 'B'
};

/*      Intervals:      1  2  3  4  5  6  7  8  9  10 11 12
 * 		Chromatic:		C  Db D  Eb E  F  Gb G  Ab A  Bb B
 * 		Major:          C     D     E  F     G     A     B
 *      Pentatonic Maj: C     D     E        G     A
 *      Penta. Min.		C        Eb    F     G        Bb
 */

Scale.MAJOR_SCALE = new Scale("Major", [1, 3, 5, 6, 8, 10, 12]);
Scale.PENTATONIC_MAJOR = new Scale("Pentatonic Major", [1, 3, 5, 8, 10]);
Scale.PENTATNOIC_MINOR = new Scale("Pentatonic Minor", [1, 4, 6, 8, 11]);

Scale.getNoteAt = function(rootNote, interval) {
	var root = Scale.CHROMATIC_NOTES[rootNote];

	var i = (root + interval - 1) % 12;
	var note = Scale.CHROMATIC_SHARP[i == 0 ? 12 : i];

	return note;
};

Scale.prototype.getName = function() {
	return this.name;
};

Scale.prototype.getType = function() {
	return this.type;
}

Scale.prototype.getIntervals = function() {
	return this.intervals;
};

Scale.prototype.getNotes = function(root, scaleBase) {
	var notes = [];
	scaleBase = typeof(scaleBase) === "undefined" ? 1 : scaleBase;
	var offset = Scale.CHROMATIC_NOTES[root];
	
	for(var i in this.intervals) {
		var interval = (this.intervals[i] + offset - scaleBase) % 12;
		var note = Scale.CHROMATIC_SHARP[interval == 0 ? 12 : interval];

		notes.push(note);
	}
	
	return notes;
};