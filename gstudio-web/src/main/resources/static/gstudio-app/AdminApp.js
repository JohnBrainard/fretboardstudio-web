function AdminApp() {
	if(AdminApp._instance) {
		throw "SingletonInstantiationError";
	}
}

AdminApp.getInstance = function() {
	if(!AdminApp._instance) {
		AdminApp._instance = new AdminApp();
	}

	return AdminApp._instance;
};

AdminApp.prototype.initialize = function() {
	var app = this;

	this.$tabs.tabs();
	this.$tabs.on("change", function(e) { app.onTabChange(e); });

	var remote = Remote.getInstance();

	remote.loadModalChordWindow(function(html) {
		app.chordWindow = new ModalChordWindow(app);
		$(html).wire(app.chordWindow);
	});
	
	remote.loadModalFingeringWindow(function(html) {
		app.fingeringWindow = new ModalFingeringWindow(app);
		$(html).wire(app.fingeringWindow);
	});
	
	remote.loadModalScaleWindow(function(html) {
		app.scaleWindow = new ModalScaleWindow(app);
		$(html).wire(app.scaleWindow);
	});

	var $noteSelects = this.$context.find(".note-select");
	for(var i in Scale.CHROMATIC_SHARP) {
		var note = Scale.CHROMATIC_SHARP[i];

		$("<option value='" + note + "'>" + note + "</option>").appendTo($noteSelects);
	}

	this.$notes.on("change", function(event) {
		app.onFingeringsTabSelected(event);
	});
	
	this.$changeNote.on("click", function(event) {
		app.onFingeringsTabSelected(event);
	});
};

AdminApp.prototype.openChord = function(chord) {
	this.chordWindow.show(chord);
};

AdminApp.prototype.openScale = function(scale) {
	console.log("Opening scale dialog for scale: " + scale);
	this.scaleWindow.show(scale);
};

AdminApp.prototype.openFingering = function(fingering) {
	this.fingeringWindow.show(fingering);
};
/*****************************************************************************
 * EVENT HANDLERS
 *****************************************************************************/
AdminApp.prototype.onTabChange = function(event) {
	if(event.target.id === "tabScales")
		this.onScaleTabSelected(event);

	if(event.target.id === "tabChords")
		this.onChordTabSelected(event);
	
	if(event.target.id === "tabFingerings")
		this.onFingeringsTabSelected(event);
};

AdminApp.prototype.onScaleTabSelected = function(event) {
	if(this.scales)
		return;

	var app = this;
	var remote = Remote.getInstance();
	var tbody = this.$tblScales.find("tbody");

	remote.loadScales(function(json, status, error) {
		for(var idx in json) {
			var scale = json[idx];

			var row = 
				"<tr>" +
					"<td>" + scale.id + "</td>" +
					"<td>" + scale.name + "</td>" +
					"<td>" + scale.intervals + "</td>" +
				"</tr>";

			$(row).appendTo(tbody)
				.data("scale", scale);
		}

		app.$tblScales.tablesorter();
		app.scales = json;

		app.$tblScales.find("tr").click(function() {
			app.openScale($(this).data("scale"));
		});
	});
};

AdminApp.prototype.onChordTabSelected = function(event) {
	if(this.chords)
		return;

	var app = this;
	var remote = Remote.getInstance();
	var tbody = this.$tblChords.find("tbody");

	tbody.html("");

	remote.loadChords(function(json, status, error) {
		var i = 0;
		for(var idx in json) {
			var chord = json[idx];

			var row = 
				"<tr>" +
					"<td>" + chord.id + "(" + i + ")</td>" +
					"<td>" + chord.name + "</td>" +
					"<td>" + chord.intervals + "</td>" +
				"</tr>";

			var $row = $(row).appendTo(tbody);
			$row.data("chord", chord);
			
			i++;
		}

		app.$tblChords.tablesorter();
		app.$tblChords.find("tr").click(function() { 
			app.openChord($(this).data("chord")); 
		});
		app.chords = json;
	});	
};

AdminApp.prototype.onFingeringsTabSelected = function(event) {
	var app = this;
	var remote = Remote.getInstance();
	var tbody = this.$tblFingerings.find("tbody");
	var note = this.$notes.val();
	
	tbody.html("");
	
	remote.loadChordFingeringsForNote(note, function(json, status, error) {
		var i = 0;
		for(var idx in json) {
			var fingering = json[idx];
			
			var notes = [];
			
			for(idy in fingering.notes) {
				notes.push(fingering.notes[idy].note.name);
			}
			
			var row =
				"<tr>" +
					"<td>" + fingering.id + "(" + i + ")</td>" +
					"<td><a class='fingering'>" + fingering.name + "</a></td>" +
					"<td>" + notes + "</td>" +
				"</tr>";

			var $row = $(row).appendTo(tbody);
			$row.data("fingering", fingering);
			$row.find("a.fingering")
				.attr("title", "Chord Fingering: " + fingering.name)
				.attr("data-placement", "right")
				.attr("rel", "popover")
				.data("fingering", fingering)
				.popover({
					html: true,
					content: function() {
						return app.getChordFingeringPopover($(this).data("fingering"));
					}
				});

			i++;
		}
		
		app.$tblFingerings.tablesorter();
		app.$tblFingerings.find("tr").click(function() {
			app.openFingering($(this).data("fingering"));
		});
	});
};

AdminApp.prototype.getChordFingeringPopover = function(fingering) {
	var $canvas = $("<canvas width='120' height='120'></canvas>");
	var canvas = new ChordCanvas($canvas.get(0), fingering);

	canvas.draw();

	return $canvas;
};