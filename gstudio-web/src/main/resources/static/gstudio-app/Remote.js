function Remote() {
	if(Remote._instance) {
		throw "SingletonInstantiationError";
	}
}

Remote.getInstance = function() {
	if(!Remote._instance) {
		Remote._instance = new Remote();
	}
	
	return Remote._instance;
};

Remote.prototype.loadScales = function(callback, target) {
	this.ajax("/api/scales", {}, callback, target);
};

Remote.prototype.loadChords = function(callback, target) {
	this.ajax("/api/chords", {}, callback, target);
};

Remote.prototype.loadTemplates = function(callback, target) {
	this.ajax("/gstudio-app/templates/templates.xml", {}, callback, target);
};

Remote.prototype.loadFingerings = function(scaleId, rootNote, callback, target) {
	var data = {
			rootNote: rootNote,
			scaleId: scaleId
	};

	this.ajax("/api/fingerings", {data:data}, callback, target);
};

/**
 * Load chords as JSON associated with the scale ID.
 * 
 * @param scaleId
 * @param callback
 * @param target
 */
Remote.prototype.loadChordsForScale = function(scaleId, callback, target) {
	this.ajax("/api/chords/scale/" + scaleId, {}, callback, target);
};

Remote.prototype.loadChordsForChord = function(chordId, callback, target) {
	this.ajax("/api/chords/chord/" + chordId, {}, callback, target);
}

Remote.prototype.loadFingeringsForChord = function(chordId, rootNote, callback, target) {
	var data = {
			rootNote: rootNote,
			chordId: chordId
	};
	
	this.ajax("/api/fingerings", {data: data}, callback, target);
};

Remote.prototype.loadChordFingeringsForNote = function(rootNote, callback, target) {
	var data = { rootNote: rootNote };
	
	this.ajax("/api/fingerings", {data: data}, callback, target);
};

Remote.prototype.loadModalChordWindow = function(callback, target) {
	var opts = { dataType: "html" };

	this.ajax("/gstudio-app/modal-chord-window.html", opts, function(data, status, xhr) {
		callback.apply(target, [data, status, null]);
	});
};

Remote.prototype.loadModalScaleWindow = function(callback, target) {
	var opts = { dataType: "html" };
	
	this.ajax("/gstudio-app/modal-scale-window.html", opts, function(data, status, xhr) {
		callback.apply(target, [data, status, null]);
	});
};

Remote.prototype.loadModalTuningWindow = function(callback, target) {
	var opts = { dataType: "html" };
	
	this.ajax("/gstudio-app/modal-tuning-window.html", opts, function(data, status, xhr) {
		callback.apply(target, [data, status, null]);
	});
};

Remote.prototype.loadTunings = function(callback, target) {
	var opts = { dataType: "xml" };

	this.ajax("/gstudio-app/tunings.xml", opts, function(data, status, xhr) {
		var tunings = new Array();

		$(data).find("tuning").each(function() {
			var name = $(this).find("name").text();
			var notes = $(this).find("notes").text().split(',');
			var desc = $(this).find("desc").text();
			var info = $(this).find("info").text();

			tunings.push(new Tuning(name, notes, desc, info));
		});

		callback.apply(target, [tunings, status, null]);
	});
};

Remote.prototype.loadModalFingeringWindow = function(callback, target) {
	var opts = { dataType: "html" };

	this.ajax("/gstudio-app/modal-fingering-window.html", opts, function(data, status, xhr) {
		callback.apply(target, [data, status, null]);
	});
};

Remote.prototype.loadSVGFretboard = function(callback, target) {
	var opts = { dataType: "xml" };
	
	this.ajax("/gstudio-app/fretboard.xml", opts, callback, target);
};

Remote.prototype.action = function(action, params, target, callback) {
	var opts = { dataType: "json", type: "POST" };
	
	this.ajax("/gstudio-app/actions.do", opts, callback, target);
};

/**
 * Generic wrapper around jQuery.ajax for use with the Remote.js API.
 * 
 * @param url
 * @param settings
 */
Remote.prototype.ajax = function(url, settings, callback, target) {
	var base = typeof(target) === 'undefined' ? this : target;

	settings.success = function(data, status, xhr) {
		callback.apply(base, [data, status, null]);
	};

	settings.error = function(xhr, status, error) {
		callback.apply(base, [null, status, error]);
	};

	jQuery.ajax(url, settings);
};
