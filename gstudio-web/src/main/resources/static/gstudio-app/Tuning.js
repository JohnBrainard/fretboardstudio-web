function Tuning(name, notes, description, info) {
	this.name = name;
	this.notes = notes;
	this.description = description;
	this.info = info;
}

Tuning.prototype.getName = function() {
	return this.name;
};

Tuning.prototype.getNotes = function() {
	return this.notes;
};

Tuning.prototype.getDescription = function() {
	return this.description;
};

Tuning.prototype.getInfo = function() {
	return this.info;
};