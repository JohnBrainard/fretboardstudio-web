function LoadProgress(loadItems) {
	this.loadItems = loadItems;
	this.errors = {};
	this.listeners = new Array();
}

/**
 * Map of items that are being loaded.
 */
LoadProgress.prototype.loadItems;

/**
 * Array of objects expecting to be notified of progress.
 */
LoadProgress.prototype.listeners;

/**
 * Call this method to indicate progress item is complete.
 * @param item
 */
LoadProgress.prototype.updateProgress = function(item) {
	if(typeof(this.loadItems[item]) === 'undefined') {
		throw("Unable to find '" + item + "' progress item.");
	}

	this.loadItems[item] = true;
	this.notifyProgressUpdate(item);

	if(this.getItemsRemaining() == 0)
		this.notifyProgressComplete();
};


LoadProgress.prototype.addError = function(item, error) {
	this.errors[item] = error;
};

/**
 * Return number of items not loaded.
 */
LoadProgress.prototype.getItemsRemaining = function() {
	var remaining = 0;

	for(var i in this.loadItems) {
		if(this.loadItems[i] == false)
			remaining ++;
	}
	
	return remaining;
};

/**
 * Returns true if errors were detected during loading.
 * 
 * @returns {Boolean}
 */
LoadProgress.prototype.hasErrors = function() {
	return this.errors.length > 0;
};

/**
 * Returns the errors. Each entry is identified by the name of the progress item.
 * 
 * @returns map of errors
 */
LoadProgress.prototype.getErrors = function() {
	return this.errors;
};

/**
 * Return the load progres.
 * 
 * @returns 1 if all items are loaded or value between 0 and 1 if there are
 *  items remaining.
 */
LoadProgress.prototype.getProgress = function() {
	var remaining = this.getItemsRemaining();

	return remaining == 0 ? 0 : this.loadItems.length / remaining;
};

LoadProgress.prototype.addListener = function(listener) {
	this.listeners.push(listener);
};

LoadProgress.prototype.notifyProgressComplete = function() {
	for(var i in this.listeners) {
		var listener = this.listeners[i];

		if(typeof(listener['progressComplete']) === 'function') {
			listener['progressComplete'].apply(listener, [this]);
		}
	}
};

LoadProgress.prototype.notifyProgressUpdate = function(item) {
	for(var i in this.listeners) {
		var listener = this.listeners[i];
		
		if(typeof(listener['progressUpdate']) === 'function') {
			listener['progressUpdate'].apply(listener, [this, item]);
		}
	}
};