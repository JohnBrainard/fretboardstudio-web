function SheetEditor() {
	
}

/*****************************************************************************
 * Initializer
 *****************************************************************************/
SheetEditor.prototype.initialize = function() {
	this.app = GStudioApp.getInstance();
	
};


/*****************************************************************************
 * Event Handlers
 *****************************************************************************/
SheetEditor.prototype.btnCloseSheet_onClick = function() {
	this.app.closeSheet();
};