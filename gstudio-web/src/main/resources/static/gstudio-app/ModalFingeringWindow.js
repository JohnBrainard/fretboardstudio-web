function ModalFingeringWindow(app) {
	// Application instance that created the ModalChordWindow dialog.
	this.app = app;
}

ModalFingeringWindow.prototype.initialize = function() {
	this.$context.modal({
		backdrop: true,
		keyboard: true,
		show: false
	});
//	this.$context.dialog({
//		modal: true,
//		autoOpen: false,
//		width: 640,
//		height: 480
//	});
};

ModalFingeringWindow.prototype.show = function(fingering) {
	var remote = Remote.getInstance();
	
	this.$title.text(fingering.name + " Chord");
	this.$context.modal("show");
};



/*****************************************************************************
 * EVENT HANDLERS
 *****************************************************************************/
ModalFingeringWindow.prototype.btnCancel_onClick = function() {
	this.$context.modal("hide");
};