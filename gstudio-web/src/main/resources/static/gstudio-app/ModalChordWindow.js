function ModalChordWindow(app) {
	// Application instance that created the ModalChordWindow dialog.
	this.app = app;
}

ModalChordWindow.prototype.initialize = function() {
	this.$context.dialog({
		modal: true,
		autoOpen: false,
		width: 640,
		height: 480
	});

	this.$notes.buttonset();
};

ModalChordWindow.prototype.show = function(chord) {
	var remote = Remote.getInstance();
	
	var note = this.$notes.find(":checked").val();
	
	remote.loadFingeringsForChord(chord.id, note, function(fingerings) {
		alert(fingerings);
	});

	this.$context
		.dialog("option", "title", chord.name + " Chord")
		.dialog("open");
};