function ModalScaleWindow(app) {
	// Application instance that created the ModalScaleWindow dialog.
	this.app = app;
}

ModalScaleWindow.prototype.initialize = function() {
	this.$context.modal( {
		backdrop: true,
		keyboard: true,
		show: false
	});
};

ModalScaleWindow.prototype.btnOkay_onClick = function() {
	this.$context.modal("hide");
};

ModalScaleWindow.prototype.btnCancel_onClick = function() {
	this.$context.modal("hide");
};

ModalScaleWindow.prototype.btnDeleteScale_onClick = function() {
	if(window.confirm("Are you sure?")) {
		alert("Deleting scale: " + this.scale.name);
		var remote = Remote.getInstance();
		var action = { type: "scale", id: this.scale.id };
		
		remote.action("delete", action, function() {
			window.location.reload();
		});

		this.$context.modal("hide");
	}
};

ModalScaleWindow.prototype.show = function(scale) {
	this.scale = scale;

	this.$context.find(".modal-header>h3").text(scale.name + " Scale");
	this.$context.modal("show");
};