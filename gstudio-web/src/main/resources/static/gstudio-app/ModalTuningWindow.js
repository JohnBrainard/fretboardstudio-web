function ModalTuningWindow(listener, tunings) {
	this.listener = listener;
	this.tunings = tunings;
}

ModalTuningWindow.prototype.initialize = function() {
	var base = this;

	this.$context.modal( {
		backdrop: true,
		keyboard: true,
		show: false
	});

	for(var i = 0; i < this.tunings.length; i++) {
		var tuning = this.tunings[i];
		var notes = tuning.getNotes();

		var $tuning = $("<li class='tuning'></li>");
		var $info = $("<h3>" + tuning.getName() + "</h3><p>" + tuning.getDescription() + "</p>")
			.appendTo($tuning);

		var $notes = $("<ul class='notes'></ul>");

		for(var n = 0; n < notes.length; n++){
			var note = notes[n];

			$("<li>" + note + "</li>").appendTo($notes);
		}

		$tuning.append($notes);
		$tuning.data("tuning", tuning).appendTo(this.$tunings);
	}

	this.$context.find("li.tuning").click(function(event) {
		base.tuningItem_onClick(this, event);
	});
	
	this.$context.bind("shown", function() {
		$(this).find(".modal-body").scrollTo(0, 0);
	});
};

ModalTuningWindow.prototype.open = function() {
	this.$context.modal("show");
};

ModalTuningWindow.prototype.btnCancel_onClick = function() {
	this.$context.modal("hide");
};

ModalTuningWindow.prototype.tuningItem_onClick = function(target) {
	var tuning = new Array();
	var tuningName = $(target).find("strong").text();
	$(target).find("ul.notes li").each(function() {
		tuning.push($(this).text().trim());
	});

	this.$context.modal("hide");

	//alert(tuningName + "(" + tuning + ") selected.");
	if(this.listener)
		this.listener.onTuningSelected(tuning, tuningName);

};