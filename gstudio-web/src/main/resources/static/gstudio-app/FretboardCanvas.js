function FretboardCanvas(canvas) {
	this.canvas = canvas;
	this.tuning = ['E', 'A', 'D', 'G', 'B', 'E'];
	//this.notes = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
	this.notes = ['C','Db','D','Eb','E','F','Gb','G','Ab','A','Bb','B'];
	this.frets = 12;
	this.intervals = {
			'C': 1,
			'C#': 2, 'Db': 2, 
			'D': 3,
			'D#': 4, 'Eb': 4,
			'E': 5,
			'F': 6,
			'F#': 7, 'Gb' : 7,
			'G': 8,
			'G#': 9, 'Ab': 9,
			'A': 10,
			'A#': 11, 'Bb': 11,
			'B': 12
	};

	var scale = Scale.PENTATONIC_MAJOR;
	var notes = scale.getNotes('C');

	this.notes = notes;

	if(window.G_vmlCanvasManager != undefined) {
		alert(G_vmlCanvasManager);
		//G_vmlCanvasManager.initElements(this.canvas);
	}
	
	// Styling info
	this.font = "normal 12px sans-serif";
	this.noteRadius = 9;

	return this;
}

/*****************************************************************************
 * Property Setters & Getters
 *****************************************************************************/
FretboardCanvas.prototype.getNotes = function() { return this.notes; };
FretboardCanvas.prototype.setNotes = function(notes) { this.notes = notes; };
FretboardCanvas.prototype.setTuning = function(tuning) { 
	this.tuning = tuning;
};

FretboardCanvas.prototype.setFont = function(font) { this.font = font; };
FretboardCanvas.prototype.setNoteRadius = function(noteRadius) { this.noteRadius = noteRadius; };

/**
 * Draws the entire fretboard on the canvas.
 * 
 */
FretboardCanvas.prototype.draw = function() {
	var base = this;
	var c = this.canvas.getContext("2d");

	this.clear(c);
	c.save();

	// Commonly used dimensions.
	var dim = new function() {
		this.x = 60; // Fretboard X position
		this.y = 10; // Fretboard Y position

		// Fretboard width & height
		this.w = base.canvas.width - this.x;
		this.h = base.canvas.height - this.y * 2;

		// Line height
		this.lh = this.h / (base.tuning.length - 1);
		
		// String width
		this.lw = this.w / (base.frets);
	}();
	
	this.drawFretboard(this.canvas, dim);
	this.drawStringLabels(this.canvas, dim);
	this.drawNotes(this.canvas, dim);

	c.restore();
};


/**
 * Clears the fretboard.
 */
FretboardCanvas.prototype.clear = function(context) {
	var c = context || this.canvas.getContext("2d");
	c.save();

	var w = this.canvas.width;
	var h = this.canvas.height;

	c.setTransform(1, 0, 0, 1, 0, 0);
	c.clearRect(0, 0, w, h);
	c.restore();
};


/**
 * Draws the grid and nut portions.
 * 
 * This method is called by FretboardCanvas.draw();
 */
FretboardCanvas.prototype.drawFretboard = function(canvas, dim) {
	var c = canvas.getContext("2d");
	var cu = new CanvasUtils(this.canvas);

	c.save();

	// Draw strings & frets.
	cu.drawGrid(dim.x, dim.y, dim.w, dim.h, this.tuning.length, this.frets + 1);

	// Draw nut
	c.lineWidth = 3.0;
	c.beginPath();

	c.moveTo(dim.x, dim.y);
	c.lineTo(dim.x, dim.h + dim.y);
	c.stroke();
	c.closePath();

	c.moveTo(dim.x + (dim.lw * (this.frets - 1)), dim.y);
	c.lineTo(dim.x + (dim.lw * (this.frets - 1)), dim.h + dim.y);
	c.stroke();
	c.closePath();

	c.restore();
};

/**
 *  Draws the string labels to the left of the nut.
 *  
 *  This method is called by FretboardCanvas.draw();
 */
FretboardCanvas.prototype.drawStringLabels = function(canvas, dim) {
	var c = canvas.getContext("2d");
	
	c.save();

	c.font = this.font;
	c.fillStyle = 'black';
	c.textAlign = 'center';
	c.textBaseline = 'middle';
	
	// Y position
	var yp = dim.y + dim.h;
	var x = 10;

	for(var i = 0; i < this.tuning.length; i++) {
		c.fillText(this.tuning[i], x, yp);
		yp -= dim.lh;
	}
};

FretboardCanvas.prototype.drawNotes = function(canvas, dim) {
	var c = canvas.getContext("2d");
	
	c.save();

	//c.font = 'normal 12px sans-serif';
	c.font = this.font;
	c.fillStyle = 'black';
	c.textAlign = 'center';
	c.textBaseline = 'middle';

	var yp = dim.y + dim.h;

	for(var sidx = 0; sidx < this.tuning.length; sidx++) {
		var string = this.tuning[sidx];		// Note name of string
		var sint = this.intervals[string];	// Note interval of string
		var xp = dim.x - dim.lw;			// Initial X position

		for(var idx in this.notes) {
			var note = this.notes[idx];
			var interval = (12 - sint + this.intervals[note]) % 12;

			var x = [ xp + (interval * dim.lw) + (dim.lw / 2) ];
			var y = yp;

			// If the note is the same as the string, draw the note on the last
			// fret as well.
			if(interval == 0) {
				x.push(x[0] + dim.w);
			}

			for(var i in x) {
				var X = x[i];
				
				if(interval == 0 && i == 0)
					X = dim.x - 18;

				c.fillStyle = '#000';
				c.beginPath();
				c.arc(X, y, this.noteRadius, 0, Math.PI * 2, true);
				c.closePath();
				//c.stroke();
				c.fill();
	
				c.fillStyle = '#fff';
				c.fillText(note, X, y + 1);
			}
		}

		yp -= dim.lh;
	}
};
