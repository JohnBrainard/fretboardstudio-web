function CanvasUtils(canvas) {
	this.canvas = canvas;

	return this;
}

CanvasUtils.prototype.clear = function(context) {
	var c = context || this.canvas.getContext("2d");
	c.save();

	var w = this.canvas.width;
	var h = this.canvas.height;

	c.setTransform(1, 0, 0, 1, 0, 0);
	c.clearRect(0, 0, w, h);
	c.restore();
};

CanvasUtils.prototype.drawGrid = function(x, y, w, h, hlines, vlines) {
	var c = this.canvas.getContext("2d");

	c.strokeRect(x, y, w, h);
	
	// Line Height
	var lh = h / (hlines - 1);
	var lw = w / (vlines - 1);
	
	// Y position
	var yp = lh + y;
	
	for(var i = 1; i < hlines - 1; i++) {
		c.beginPath();
		c.moveTo(x, yp);
		c.lineTo(x+w, yp);
		c.stroke();
		c.closePath();
		yp += lh;
	}

	var xp = lw + x;
	for(var i = 0; i < vlines - 1; i++) {
		c.beginPath();
		c.moveTo(xp, y);
		c.lineTo(xp, y+h);
		c.stroke();
		c.closePath();
		xp += lw;
	}
};

/**
 * Draws a simple line.
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 */
CanvasUtils.prototype.drawLine = function(x1, y1, x2, y2) {
	var c = this.canvas.getContext("2d");
	
	c.beginPath();
	c.moveTo(x1, y1);
	c.lineTo(x2, y2);
	c.stroke();
	c.closePath();
};

/**
 * Draws a circle.
 * @param x
 * @param y
 * @param radius
 */
CanvasUtils.prototype.drawCircle = function(x, y, radius) {
	var c = this.canvas.getContext("2d");
	c.beginPath();
	c.arc(x, y, radius, 0, Math.PI * 2, false);
	c.stroke();
};


/**
 * Fills a circle.
 * @param x
 * @param y
 * @param radius
 */
CanvasUtils.prototype.fillCircle = function(x, y, radius) {
	var c = this.canvas.getContext("2d");
	c.beginPath();
	c.arc(x, y, radius, 0, Math.PI * 2, false);
	c.fill();
};


/**
 * Draws a series of lines on the canvas.
 * 
 * @param x horizontal starting point
 * @param y vertical starting point
 * @param w width
 * @param h height
 * @param numLines number of lines to draw
 * @param orientation line orientation ('vertical', 'horizontal')
 */
CanvasUtils.prototype.drawLines = function(x, y, w, h, numLines, orientation) {
	var c = this.canvas.getContext("2d");

	var lw = w / (numLines - 1); // Used for vertical lines
	var lh = h / (numLines - 1); // Used for horizontal lines

	for(var i = 0; i < numLines; i++) {
		c.beginPath();
		var x1 = 0, x2 = 0, y1 = 0, y2 = 0;

		if(orientation === "vertical") {
			x1 = x + (lw * i);
			x2 = x1;
			y1 = y;
			y2 = y+h;
		} else if(orientation === "horizontal") {
			x1 = x;
			x2 = x+w;
			y1 = y + (lh * i);
			y2 = y1;
		}

		c.moveTo(x1, y1);
		c.lineTo(x2, y2);
		c.stroke();
		c.closePath();
	}
};