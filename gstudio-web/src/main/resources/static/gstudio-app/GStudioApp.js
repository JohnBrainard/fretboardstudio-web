/**
 * GStudioApp is a singleton class for the purposes of managing the application
 * state. Use GStudioApp.getInstance() to return the main instance.
 */
var GStudioApp = function() {
	if(GStudioApp.instance != null) {
		throw "Use GStudioApp.getInstance() to ensure only one instance is created.";
	}

	// Used for storing data in the browser.
	this.storage = new StorageWrapper();
};

// We only want one instance.
GStudioApp.instance = null;
GStudioApp.getInstance = function() {
	//return new GStudioApp();
	if(GStudioApp.instance === null) {
		GStudioApp.instance = new GStudioApp();
	}

	return GStudioApp.instance;
};

GStudioApp.prototype.initialize = function() {
	var app = this;

	// Init progress tracker.
	this.progress = new LoadProgress( {
		'templates': false,
		'scales': false,
		'chords': false,
		'tuning-window': false
	});

	this.progress.addListener(this);

	// Feature support check.
	this.svgSupported = document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1");
	if(!this.isBrowserSupported()) {
		this.$referenceTools.hide();
		this.$pageContent.removeClass("hide").show();
		this.$oldBrowser.removeClass("hide").show();

		return false;
	}

	if(!this.svgSupported) {
		this.$homeFretboard = this.$pageContent.find("div[name='fretboard']");
		this.$homeFretboard.fretboard({
				width : 820,
				height : 150
		});
		
		this.$svgFretboard.css("display", "none");
	} else {
		this.$canvasFretboard.css("display", "none");
	}


	// UI elements initialization.
	this.$notes.buttonset();
	this.$tabs.tabs();
	this.$tabs.on("change", function(event) { return app.onTabChanged(event); });

	this.$context.find(".mnu-tuning").click(function() {
		app.onTuningChanged(this); 
		app.$navigation.find('li').removeClass('open');
		return false;
	});

	var remote = Remote.getInstance();

	remote.loadChords(this.chordsLoaded, this);
	remote.loadScales(this.scalesLoaded, this);
	remote.loadTemplates(this.templatesLoaded, this);
	remote.loadTunings(this.tuningsLoaded, this);
	//remote.loadModalTuningWindow(this.tuningWindowLoaded, this);


	this.svgFretboard = new FretboardSVGCanvas(this.$svgFretboard, {});
	var tuning = this.storage.getObject("tuning");
	if(tuning)
		this.svgFretboard.setTuning(tuning);

	this.svgFretboard.initialize();

};

/**
 * Performs some checks to determine if the users's browser is supported.
 */
GStudioApp.prototype.isBrowserSupported = function() {
	var canvas = document.createElement("canvas");

	if(! canvas.getContext )
		return false;

	return true;
};

/**
 * Updates the main fretboard with the current chord or scale selection.
 * 
 * Calling update causes the tab content to be invalidated resulting in the data being
 * fetched from the server the first time each tab is made visible.
 */
GStudioApp.prototype.update = function() {
	this.invalidateTabContent();

	if(this.getProgress().getItemsRemaining() > 0)
		return;

	this.scale = this.scales[this.$scales.val()];
	if (this.scale === undefined) {
		this.chord = this.chords[this.$scales.val()];
	}

	this.updateFretboard();

	if(this.getVisibleTab() === "tabChordDiagrams")
		this.updateChordFingerings();

	else
		this.updateChordScales();

	this.$chordDiagrams.fadeOut(100);
};

GStudioApp.prototype.updateFretboard = function() {
	var note = this.$notes.find("input[type='radio']:checked").val();
	var scale = this.scale !== undefined ? this.scale : this.chord

	if(!this.svgSupported) {
		this.$homeFretboard.fretboard("notes", scale.getNotes(note));
	} else {
		this.svgFretboard.setNotes(scale.getNotes(note));
		this.svgFretboard.drawNotes();
	}
};

/**
 * Invalidates the contents of the tabs. This will help to ensure only the necessary
 * data is loaded from the server when changing tabs.
 */
GStudioApp.prototype.invalidateTabContent = function() {
	// Invalidate chord fingering data...
	this.$chordDiagrams.html('')
		.data("valid", false);

	this.$chordScales.html('')
		.data("valid", false);
};

GStudioApp.prototype.updateChordFingerings = function() {
	var note = this.$notes.find("input[type='radio']:checked").val();

	var id = this.scale !== undefined ? this.scale.id : this.chord.id

	if (this.scale !== undefined) {
		Remote.getInstance().loadFingerings(this.scale.id, note, this.onChordFingeringsLoaded, this);
	} else if (this.chord !== undefined) {
		Remote.getInstance().loadFingeringsForChord(this.chord.id, note, this.onChordFingeringsLoaded, this);
	}
};

GStudioApp.prototype.updateChordScales = function() {
	if (this.scale !== undefined)
		Remote.getInstance().loadChordsForScale(this.scale.id, this.onChordScalesLoaded, this);
	else if (this.chord !== undefined)
		Remote.getInstance().loadChordsForChord(this.chord.id, this.onChordScalesLoaded, this);
};

GStudioApp.prototype.updateChordScaleTunings = function() {
	var tuning = this.getTuning();
	
	this.$chordScales.find(".fretboard").each(function() {
		var fretboard = $(this).data("fretboard");
		fretboard.setTuning(tuning);
		fretboard.draw();
	});
};

/*****************************************************************************
 * Property Getters & Setters
 *****************************************************************************/
GStudioApp.prototype.getScales = function() { return this.scales; };
GStudioApp.prototype.getChords = function() { return this.chords; };
GStudioApp.prototype.getTemplates = function() { return this.templates; };

GStudioApp.prototype.getAppLocation = function() { return this.appLocation; };
GStudioApp.prototype.setAppLocation = function(appLocation) { this.appLocation = appLocation; };

GStudioApp.prototype.setNewSheetDialog = function(newSheetDialog) {
	this.newSheetDialog = newSheetDialog;

	this.newSheetDialog.addListener(this);
};

GStudioApp.prototype.setCurrentSheet = function(sheet) {
	if(sheet == null)
		this.storage.removeObject("currentSheet", "session");
	else
		this.storage.setObject("currentSheet", sheet.getData(), "session");
};

GStudioApp.prototype.getCurrentSheet = function() {
	var data = this.storage.getObject("currentSheet", "session");
	
	if(!data)
		return null;
	
	var sheet = new Sheet(data);
//	sheet.initialize();
	
	return sheet;
};

GStudioApp.prototype.getTuning = function() {
	var tuning = this.storage.getObject("tuning");

	return tuning ? tuning : ["E", "A", "D", "G", "B", "E"];
};

GStudioApp.prototype.getVisibleTab = function() {
	return this.$tabs.find(".active a").attr("id");
};

/*****************************************************************************
 * Methods
 *****************************************************************************/
GStudioApp.prototype.switchToSheet = function(sheet) {
	this.$pageContent.hide('fade');
	this.$sheetContent.show('fade');

	this.editor = new SheetEditor(sheet);
	this.$sheetContent.wire(this.editor);

	this.setCurrentSheet(sheet);

	var template = sheet.getTemplate();
	var url = this.getAppLocation() + "/templates/" + template.file;
	//var template = this.getAppLocation() + '/templates/' + sheet.getTemplate().file;
	this.ajax(url, {
		dataType: "html",
		callback: this.onSheetLoaded
	});
};

GStudioApp.prototype.closeSheet = function() {
	this.$sheetContent.hide('fade');
	this.$pageContent.show('fade');

	this.setCurrentSheet(null);
};


/*****************************************************************************
 * AJAX Calls
 *****************************************************************************/

/**
 * Get the progress instance to allow external resources to be notified of progress updates.
 * 
 * @returns
 */
GStudioApp.prototype.getProgress = function() {
	return this.progress;
};

// UI Events....
GStudioApp.prototype.scales_changed = function() {
	this.scale = this.scales[this.$scales.val()];

	this.update();
};

GStudioApp.prototype.chords_changed = function() {
	this.scale = this.chords[this.$chords.val()];

	this.update();
};

GStudioApp.prototype.notes_changed = function() {
	this.update();
};

GStudioApp.prototype.mnuNewSheet_onClick = function() {
	this.newSheetDialog.show();

	return false;
};

GStudioApp.prototype.mnuMoreTunings_onClick = function() {
	this.tuningWindow.open();

	return false;
};

GStudioApp.prototype.onTuningChanged = function(menu) {
	var strings = $(menu).attr("tuning").split(',');

	this.storage.setObject("tuning", strings);
	this.svgFretboard.setTuning(strings);

	this.updateChordScaleTunings();
};

GStudioApp.prototype.onTuningSelected = function(tuning, name) {
	this.storage.setObject("tuning", tuning);
	this.svgFretboard.setTuning(tuning);

	this.updateChordScaleTunings();
};

GStudioApp.prototype.onTabChanged = function(event) {
	switch(event.target.id) {
	
	case "tabChordScales":
		if(!this.$chordScales.data("valid"))
			this.updateChordScales();
		
		break;
		
	case "tabChordDiagrams":
		if(!this.$chordDiagrams.data("valid"))
			this.updateChordFingerings();
	
	}

	//this.update();
};

GStudioApp.prototype.mnuGuitar_onClick = function() {
	this.svgFretboard.setTuning(['E', 'A', 'D', 'G', 'B', 'E']);
};

GStudioApp.prototype.onNewSheet = function(newSheetDialog, options) {
	var storage = new StorageWrapper();

	var sheet = new Sheet(options);
	storage.setObject("gsheets.sheet." + options.name, sheet.getData());

	this.switchToSheet(sheet);
};

GStudioApp.prototype.onSheetLoaded = function(html, status, error) {
	this.$sheet.html(html);

	this.$sheet.wire(this.getCurrentSheet());
};

GStudioApp.prototype.onChordFingeringsLoaded = function(json, status, error) {
	this.$chordDiagrams.html('');
	var rootNote;

	for(var id in json) {
		var fingering = json[id];

		if(rootNote === undefined || fingering.rootNote.name !== rootNote.name) {
			var name = fingering.rootNote.name;
			if (fingering.rootNote.flatName !== "" && fingering.rootNote.name !== fingering.rootNote.flatName)
				name = name +  " / " + fingering.rootNote.flatName;

			$("<div class='rootNote'><h2>" + name + "</h2></div>").appendTo(this.$chordDiagrams);
			rootNote = fingering.rootNote;
		}

		var $canvas = $("<canvas width='100' height='110'></canvas>");

		var chordDiagram = new ChordCanvas($canvas.get(0), fingering);
		chordDiagram.draw();

		var $item = $("<div class='thumbnail'></div>");

		$item.append("<span style='display: block;'>" + fingering.name + "</span>");
		$item.append($canvas);
		$item.appendTo(this.$chordDiagrams);
	}

	this.$chordDiagrams.data("valid", true).show('fade');
};

GStudioApp.prototype.onChordScalesLoaded = function(json, status, error) {
	this.$chordScales.html('');
	var rootNote = this.$notes.find("input[type='radio']:checked").val();
	var tuning = this.getTuning();

	for(var i in json) {
		var interval = json[i];
		var note = Scale.getNoteAt(rootNote, parseInt(interval.interval) + 1);

		$("<div class='rootNote'><h2>" + note + "</h2></div>").appendTo(this.$chordScales);

		for(var idx in interval.chords) {
			var chord = interval.chords[idx];
			var scale = new Scale(chord.id, chord.name, chord.intervals, "chord");

			var $canvas = $("<canvas width='376' height='100'></canvas>");
			var fretboard = new FretboardCanvas($canvas.get(0));

			fretboard.setNotes(scale.getNotes(note, 0));
			fretboard.setTuning(tuning);
			fretboard.setFont("normal 10px sans-serif");
			fretboard.setNoteRadius(7);
			fretboard.draw();

			var $item = $("<div class='thumbnail fretboard'></div>");

			$item.append("<span style='display: block;'>" + note + " " + chord.name + "</span>");
			$item.append($canvas);
			$item.appendTo(this.$chordScales);
			
			$item.data("fretboard", fretboard);
		}
	}

	this.$chordScales.data("valid", true).show('fade');
};
/**
 * Callback function for AJAX routine returning templates.
 * 
 * Sample XML format:
 * <templates>
 *	<template>
 *		<name>Simple</name>
 *		<file>simple.html</file>
 *		<description>
 *			Simple page layout with a full width fretboard diagram on the top. 
 *		</description>
 *	</template>
 * </templates>
 * 
 * @param xml XML data object.
 * @param status Response status
 * @param error Error. Null if AJAX call was successful.
 */
GStudioApp.prototype.templatesLoaded = function(xml, status, error) {
	var templates = {};

	if(error) {
		alert("Unable to load templates: " + error);
		return;
	}

	jQuery(xml).find("template").each(function() {
		var template = {
				name: $(this).children("name").text(),
				file: $(this).children("file").text(),
				description: $(this).children("description").text()
		};
		
		templates[template.name] = template;		
	});
	
	this.templates = templates;
	this.progress.updateProgress("templates");
};

/**
 * 
 * @param xml
 * @param status
 * @param error
 */
GStudioApp.prototype.scalesLoaded = function(json, status, error) {
	var scales = {};
	
	if(error) {
		this.progress.addError("scales", "Unable to load scales: " + error);
		return;
	}

	for(var key in json) {
		var name = json[key].name;
		var id = json[key].id;
		var intervals = json[key].intervals;

		for(var i = 0; i < intervals.length; i++)
			intervals[i]++;

		scales[key] = new Scale(id, name, intervals);
	}

	this.scales = scales;
	this.progress.updateProgress("scales");
};


/**
 * 
 * @param xml
 * @param status
 * @param error
 */
GStudioApp.prototype.chordsLoaded = function(json, status, error) {
	var chords = {};

	if(error) {
		this.progress.addError("chords", "Unable to load chords: " + error);
		return;
	}

	for(var key in json) {
		var chord = json[key];
		var intervals = json[key].intervals;

		for(var i = 0; i < intervals.length; i++)
			intervals[i]++;

		chords[key] = new Scale(chord.id, chord.name, intervals, "chord");
	}

	this.chords = chords;
	this.progress.updateProgress("chords");
};

GStudioApp.prototype.tuningsLoaded = function(tunings, status, error) {
	var remote = Remote.getInstance();

	this.tunings = tunings;
	remote.loadModalTuningWindow(this.tuningWindowLoaded, this);
};

/**
 * Called when Remote.js finishes loading the tuning window HTML
 * 
 * @param html
 * @param status
 * @param error
 */
GStudioApp.prototype.tuningWindowLoaded = function(html, status, error) {
	this.tuningWindow = new ModalTuningWindow(this, this.tunings);

	$(html).wire(this.tuningWindow);
	this.progress.updateProgress("tuning-window");
};


/**
 * Called when all the items are completely loaded.
 */
GStudioApp.prototype.progressComplete = function() {
	var currentSheet = this.getCurrentSheet();

	var $scales = this.$scales.append("<optgroup label='Scales'></optgroup>");
	for(var idx in this.scales) {
		var scale = this.scales[idx];
		
		$scales.append("<option value='" + scale.getName() + "'>" + scale.getName() + "</option>");
	}
	
	var $chords = this.$scales.append("<optgroup label='Chords'></optgroup>");
	for(var idx in this.chords) {
		var chord = this.chords[idx];
		
		$chords.append("<option value='" + chord.getName() + "'>" + chord.getName() + "</option>");
	}
	
	if(currentSheet)
		this.switchToSheet(currentSheet);

	else
		this.$pageContent.show('fade');

	this.update();
};

GStudioApp.prototype.progressUpdate = function(progress, item) {
	if(item === 'templates' && this.$templates) {
		for(var idx in this.templates) {
			var template = this.templates[idx];
			
			this.$templates.append("<option value='" + template.name + "'>" + template.name + "</option>");
		}
	}
};


/**
 * Wrapper around jQuery.ajax. Callbacks are executed within the GStudioApp scope.
 * 
 * @param url
 * @param settings
 */
//GStudioApp.prototype.ajax = function(url, settings) {
//	var base = this;
//	var callback = settings.callback;
//
//	settings.success = function(data, status, xhr) {
//		callback.apply(base, [data, status, null]);
//	};
//
//	settings.error = function(xhr, status, error) {
//		callback.apply(base, [null, status, error]);
//	};
//
//	jQuery.ajax(url, settings);
//};

// Fields/Properties
GStudioApp.prototype.$canvas = null;
