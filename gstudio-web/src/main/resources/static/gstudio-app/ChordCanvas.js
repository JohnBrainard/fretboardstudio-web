function ChordCanvas(canvas, options) {
	this.canvas = canvas;

	this.options = jQuery.extend({}, ChordCanvas.defaults, options);
}

/*****************************************************************************
 * Default Options
 *****************************************************************************/
ChordCanvas.defaults = {
		/**
		 * The starting fret. If 0, the nut will be drawn, otherwise,
		 * first fret will be drawn as normal.
		 */
		fret: 0,
		
		/**
		 * Number of frets to display.
		 */
		frets: 5,
		
		/**
		 * Number of strings to display.
		 */
		strings: 6
};

/*****************************************************************************
 * Property Setters & Getters
 *****************************************************************************/
ChordCanvas.prototype.getOptions = function() { return this.options; };
ChordCanvas.prototype.setOptions = function(options) { this.options = options; };


/*****************************************************************************
 * Drawing Methods
 *****************************************************************************/
ChordCanvas.prototype.draw = function() {
	var base = this;
	var c = this.canvas.getContext("2d");
	var cu = new CanvasUtils(this.canvas);

	cu.clear();
	c.save();

	// Commonly used dimensions
	var dim = new function() {
		this.x = 10;
		this.y = 20;

		this.w = base.canvas.width - this.x * 2 - 10;
		this.h = base.canvas.height - this.y;

		this.lh = this.h / (base.options.frets);
		this.lw = this.w / (base.options.strings - 1);
	}();

	// Set up font information.
	c.font = 'bold 10px sans-serif';
	c.fillStyle = 'black';
	c.textAlign = 'center';
	c.textBaseline = 'middle';

	this.drawFretboard(dim);
	this.drawChord(dim);
};

ChordCanvas.prototype.drawFretboard = function(dim) {
	var c = this.canvas.getContext("2d");
	var cu = new CanvasUtils(this.canvas);

	// If the first fret isn't the nut, draw the strings through
	// the fret. Otherwise, strings should stop at the nut to indicate
	// fret position 0
	var yoffset = (this.options.fret == 1 ? 0 : 3);

	// Draw strings...
	cu.drawLines(dim.x, dim.y - yoffset, dim.w, dim.h, this.options.strings, "vertical");

	// Draw frets...
	cu.drawLines(dim.x, dim.y, dim.w, dim.h - (dim.h/this.options.frets), this.options.frets, "horizontal");
	if(this.options.fret == 1) {
		c.strokeWidth = 2;
		cu.drawLine(dim.x, dim.y, dim.x + dim.w, dim.y);
	} else {
		var x = dim.x + dim.w + 12;
		var y = dim.y + 10;
		c.fillText(this.options.fret, x, y);
	}
};

ChordCanvas.prototype.drawChord = function(dim) {
	var c = this.canvas.getContext("2d");
	var cu = new CanvasUtils(this.canvas);
//	var chord = this.options.chord;

	for(var idx in this.options.notes) {
		var note = this.options.notes[idx];
		var fret = note.fret - this.options.fret;

		// Draw note name at nut...
		var x = (dim.lw * note.string) + dim.x;
		var y = dim.y - 10;

		if(typeof(note.note) !== 'undefined')
			c.fillText(note.note.name === 'X' ? String.fromCharCode(215) : note.note.name, x, y);

		// Draw note circle...
		y = dim.y + (fret * dim.lh) + (dim.lh / 2);
		
		if(fret >= 0) {
			if(note.optional)
				cu.drawCircle(x, y, 5);

			else
				cu.fillCircle(x, y, 5);
		}
	}
};