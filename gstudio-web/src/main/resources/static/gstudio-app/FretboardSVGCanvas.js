function FretboardSVGCanvas(container, options) {
	this.container = container;
	this.$container = $(container);

	this.intervals = {
			'C': 1,
			'C#': 2, 'Db': 2, 
			'D': 3,
			'D#': 4, 'Eb': 4,
			'E': 5,
			'F': 6,
			'F#': 7, 'Gb' : 7,
			'G': 8,
			'G#': 9, 'Ab': 9,
			'A': 10,
			'A#': 11, 'Bb': 11,
			'B': 12
	};

	this.opt = $.extend({
		fretboardSVG: "/gstudio-app/fretboard.xml",
		tuning: ['E', 'A', 'D', 'G', 'B', 'E'],
		frets: 12,
		width: 820,
		height: 150
	}, options);
}

FretboardSVGCanvas.prototype.initialize = function() {
	var base = this;
	
	this.$container.svg({
		loadURL: this.opt.fretboardSVG,
		onLoad: function(svg) {
			base.svg = svg; 
			base.frets = svg.group(svg.root(), "frets");
			base.strings = svg.group(svg.root(), "strings");
			base.labels = svg.group(svg.root(), "labels");
			base.notes = svg.group(svg.root(), "notes");
			base.draw(); }
	});
};

FretboardSVGCanvas.prototype.setNotes = function(notes) {
	this.opt.notes = notes;
};

FretboardSVGCanvas.prototype.setTuning = function(tuning, redraw) {
	this.opt.tuning = tuning;

	if(typeof(redraw) === 'undefined' || redraw === true) {
		this.redraw();
	}
};

FretboardSVGCanvas.prototype.setStrings = function(strings, redraw) {
	this.opt.strings = strings;
	
	if(typeof(redraw) === 'undefined' || redraw === true) {
		$(this.labels).empty();
		
		this.drawStringLabels(this.dim);
		this.drawNotes();
	}
};

FretboardSVGCanvas.prototype.getDimensions = function() {
	var base = this;

	return new function() {
		this.x = 60; // Fretboard X position
		this.y = 20; // Fretboard Y position

		// Fretboard width & height
		this.w = base.opt.width - this.x;
		this.h = base.opt.height - this.y * 2;

		// Line height
		this.lh = this.h / (base.opt.tuning.length - 1);
		
		// String width
		this.lw = this.w / (base.opt.frets);
	}();
};

FretboardSVGCanvas.prototype.redraw = function() {
	if(!this.svg)
		return;

	$(this.strings).empty();
	$(this.frets).empty();
	$(this.labels).empty();
	$(this.notes).empty();
	
	this.draw();
};

FretboardSVGCanvas.prototype.draw = function() {
	var dim = this.getDimensions();

	this.drawFretboard(dim);
	this.drawStringLabels(dim);
	this.drawNotes(dim);
};

FretboardSVGCanvas.prototype.drawFretboard = function(dim) {
	var svg = this.svg;

	for(var i = 1; i < this.opt.tuning.length - 1; i++) {
		var string = svg.line(this.strings, 
				dim.x,
				dim.y + (i * dim.lh),
				dim.x + dim.w,
				dim.y + (i * dim.lh),
				{class: "string"});
	}

	// Draw frets...
	for(var i = 1; i < this.opt.frets; i++) {
		var fret = svg.line(this.frets,
			dim.x + (i * dim.lw),
			dim.y,
			dim.x + (i * dim.lw),
			dim.y + dim.h,
			{ class: "fret" });
	}

	// Draw fret markers
	var markers = [3, 5, 7, 9, 12];
	for(var idx in markers) {
		var i = markers[idx];
		var marker = svg.circle(
				this.frets,
				dim.x + (i * dim.lw) - (dim.lw / 2),
				dim.y + dim.h + 15,
				2,
				{class: "marker"}
		);
	}
	
	// Draw fret numbers
	for(var i = 1; i < this.opt.frets; i++) {
		svg.text(this.frets, 
				dim.x + (i * dim.lw),
				dim.y + dim.h + 18,
				"" + i, { class: "fret-label" });
	}
};

/**
 * Draws the string labels to the left of the fretboard.
 * 
 * @param dim
 */
FretboardSVGCanvas.prototype.drawStringLabels = function(dim) {
	var svg = this.svg;
	
	var yp = dim.y + dim.h;
	var x = 10;

	for(var i = 0; i < this.opt.tuning.length; i++) {
		var string = this.opt.tuning[i];

		svg.text(this.labels, x, yp, string, { class: "string-label" });
		yp -= dim.lh;
	}
};

FretboardSVGCanvas.prototype.drawNotes = function(dim) {
	dim = typeof(dim) === 'undefined' ? this.getDimensions() : dim;

	var yp = dim.y + dim.h;

	var svg = this.svg;
	//var notes = svg.group(svg.root(), "notes");
	$(this.notes).empty();

	for(sidx = 0; sidx < this.opt.tuning.length; sidx++) {
		var string = this.opt.tuning[sidx];
		var sint = this.intervals[string];
		var xp = dim.x - dim.lw;
		
		for(var idx in this.opt.notes) {
			var note = this.opt.notes[idx];
			var interval = (12 - sint + this.intervals[note]) % 12;
			
			var x = [ xp + (interval * dim.lw) + (dim.lw / 2) ];
			var y = yp;
			
			// If the note is the same as the string, draw the note on the last fret
			// as well.
			if(interval == 0)
				x.push(x[0] + dim.w);
			
			for(var i in x) {
				var X = x[i];

				if(interval == 0 && i == 0)
					X = dim.x - 18; // Draw the note at the nut closer to the nut.

				var label = svg.createText()
					.string(note.substring(0,1));
				
				if(note.length == 2)
					label.span(note.substring(1), {class:"accidental"});

				var circle = svg.circle(this.notes, X, y, 9, {class: "note note_" + note});
				var enote = svg.text(this.notes, X, y + 4, label, {class: "note-label note_" + note});
				
				if(note == this.opt.notes[0])
					$(circle).addClass("root");

			}
		}
		
		yp -= dim.lh;
	}
};