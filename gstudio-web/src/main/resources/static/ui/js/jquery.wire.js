/**
 * Wire is a framework for auto-wiring components and event handlers.
 */

(function( $ ) {

	var methods = {
		init: function(object) {

			return this.each( function () {
				var $this = $(this);
				object["$context"] = $this;

				$this.find("[id]").each(function() {
					var $ele = $(this);
					var id = $ele.attr("id");

					methods._initElement($this, object, id, $ele);

					if(object[id + "_onClick"])
						$ele.bind("click", methods._bindHandler(object, id+"_onClick"));

					if(object[id + "_changed"])
						$ele.bind("change", methods._bindHandler(object, id+"_changed"));

					if(object[id + "_keyUp"])
						$ele.bind("keyup", methods._bindHandler(object, id+"_keyUp"));
					
					if(object[id + "_keyDown"]) 
						$ele.bind("keydown", methods._bindHandler(object, id+"_keyDown"));
				});
				
				methods._initTokens($this, object);
				methods._initButtons($this);

				if(typeof object["initialize"] === 'function')
					object.initialize();
			});
		},
		_initTokens: function($this, object) {
			$this.find("[token]").each(function() {
				$(this).text(object.tokens[$(this).attr("token")]);
			});
		},
		_initButtons: function($this) {
			if(typeof $.fn.button === 'function')
				$("button,.button,input[type='submit']", $this).button();
		},
		_initElement: function($this, obj, id, ele) {
			obj["$" + id] = ele;
		},
		_bindHandler: function(object, fn) {
			return function() { return object[fn](); };
		},
		_findElementsWithIds: function() {
			return this.find("[id]");
		}
	};

	
	$.fn.wire = function ( method ) {

		if( methods[method] ) {
			return methods[method].apply(this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' + method  + ' does not exist on jQuery.wire.');
		}

	};
})(jQuery);