function StorageWrapper() {
	
};

StorageWrapper.prototype.getStorage = function(scope) {
	var storage;
	
	if(typeof(scope) === 'undefined' || scope === 'local')
		storage = window.localStorage;
	
	else
		storage = window.sessionStorage;
	
	return storage;
};

StorageWrapper.prototype.setObject = function(key, value, scope) {
	if(typeof(value) === 'undefined')
		return false;

	var storage = this.getStorage(scope);
	var json = JSON.stringify(value);

	try {
		storage.setItem(key, json);
	} catch (error) {
		return false;
	}

	return true;
};

StorageWrapper.prototype.getObject = function(key, scope) {
	var storage = this.getStorage(scope);
	var json = storage.getItem(key);

	if (typeof (json) === 'undefined')
		return false;

	try {
		return JSON.parse(json);
	} catch (error) {
		return false;
	}
};

StorageWrapper.prototype.removeObject = function(key, scope) {
	var storage = this.getStorage(scope);
	
	storage.removeItem(key);
};