/**
 * This jQuery plugin serves as a convenient wrapper around the FretboardCanvas
 * class. This allows you to easily insert fretboard diagrams into your web page
 * by passing a basic set of options into the selector.
 * 
 * $("#fretboard").fretboard({ key: 'A', scale: 'major' });
 */

(function($, window, document, undefined) {
	var pluginName = 'fretboard';
	var defaults = {
		width: 150,
		height: 75,

		key: 'C',
		scale: 'major',
		strings: 6,
		tuning: ['E', 'A', 'D', 'G', 'B', 'E']
	};
	
	function Fretboard( element, options ) {
		this.element = element;

		this.options = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;
		
		this.init();
	}

	Fretboard.prototype.init = function() {
		this.canvas = $("<canvas></canvas>")
			.attr("width", this.options.width)
			.attr("height", this.options.height)
			.appendTo(this.element);

		this.fretboard = new FretboardCanvas(this.canvas.get(0));
		this.fretboard.draw();
	};
	
	/**
	 * Update the fretboard scale.
	 */
	Fretboard.prototype.notes = function(notes) {
		this.fretboard.notes = notes;
		this.fretboard.draw();
	};
	
	$.fn[pluginName] = function ( arg ) {
		var args = Array.prototype.slice.call(arguments, 1);

		return this.each(function() {
			var fretboard = $.data(this, 'plugin_fretboard');
			
			if(!fretboard)
				$.data(this, 'plugin_fretboard', new Fretboard( this, arg ));

			else {
				var arg1 = arguments[1];

				if(typeof(arg) === 'object')
					fretboard.options(arg);

				else if(fretboard[arg])
					fretboard[arg].apply(fretboard, args);

			}
		});
	};
}) (jQuery);