package com.fretboardstudio.web

import com.fretboardstudio.ChordFingering
import com.fretboardstudio.Note
import com.fretboardstudio.dao.ChordDAO
import com.fretboardstudio.dao.ChordFingeringDAO
import com.fretboardstudio.dao.ScaleDAO
import com.fretboardstudio.util.ChordScaleMapUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/api/fingerings")
class FingeringsController @Autowired constructor(val chordDAO: ChordDAO, val scaleDAO: ScaleDAO, val fingeringDAO: ChordFingeringDAO) {

	private val csMapUtil = lazy {
		ChordScaleMapUtil(chordDAO.findAll())
	}

	@RequestMapping
	fun fingerings(
			@RequestParam(value="rootNote", required=false) rootNote: String?,
			@RequestParam(value="scaleId", required=false) scaleId: String?,
			@RequestParam(value="chordId", required=false) chordId: String?):List<ChordFingering> {

		val note = Note.getNote(rootNote)
		var intervals:IntArray? = null

		if (scaleId != null) {
			val scale = scaleDAO.get(scaleId)
			intervals = scale.intervals
		} else if (chordId != null) {
			val chord = chordDAO.get(chordId)
			intervals = chord.intervals
		}

		if (intervals != null) {
			val scaleChords = csMapUtil.value.findChords(intervals)

			val result = ArrayList<ChordFingering>()
			scaleChords.forEach {
				val fingerings = fingeringDAO.findAllForChords(note.getNoteAt(it.interval), it.chordIds)
				result.addAll(fingerings)
			}

			return result
		}

		return emptyList()
	}

}