package com.fretboardstudio.web.config

import com.fretboardstudio.dao.config.DaoConfig
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import


@Configuration
@Import(DaoConfig::class)
open class FretboardStudioConfig
