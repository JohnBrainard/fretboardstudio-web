package com.fretboardstudio.web

import com.fretboardstudio.dao.ScaleDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
 * Created by John on 10/7/15.
 */

@Controller
class HomeController {
	@Autowired
	lateinit var scaleDAO: ScaleDAO

	@RequestMapping("/")
	public fun index():String {
		return "index"
	}

	@RequestMapping("/about")
	public fun about() = "about"
}