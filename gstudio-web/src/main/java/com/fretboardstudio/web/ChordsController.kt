package com.fretboardstudio.web

import com.fretboardstudio.ScaleChords
import com.fretboardstudio.dao.ChordDAO
import com.fretboardstudio.dao.ScaleDAO
import com.fretboardstudio.util.ChordScaleMapUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("/api/chords")
class ChordsController @Autowired constructor(private val chordDAO: ChordDAO, private val scaleDAO: ScaleDAO) {
	var chords = lazy {
		chordDAO.findAll().associateBy { it.name }
	}

	var csMapUtil = lazy {
		ChordScaleMapUtil(chordDAO.findAll())
	}

	@RequestMapping(produces = arrayOf("application/json"))
	fun chords() = chords.value

	@RequestMapping(value= ["/scale/{scaleId}"], produces = arrayOf("application/json"))
	fun chordsForScale(@PathVariable("scaleId") scaleId:String):List<ScaleChords> {
		val scale = scaleDAO.get(scaleId)
		return csMapUtil.value.findChords(scale.intervals)
	}

	@RequestMapping(value = ["/chord/{chordId}"], produces = arrayOf("application/json"))
	fun chordsForChord(@PathVariable("chordId") chordId: String): List<ScaleChords> {
		val chord = chordDAO.get(chordId)
		return csMapUtil.value.findChords(chord.intervals)
	}
}