package com.fretboardstudio.web

import com.fretboardstudio.dao.ScaleDAO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/scales")
class ScalesController @Autowired constructor(private val scaleDAO: ScaleDAO) {
	private var scales = lazy {
		scaleDAO.findAll().associateBy { it.name }
	}

	@RequestMapping(produces = arrayOf("application/json"))
	fun scales() = scales.value
}