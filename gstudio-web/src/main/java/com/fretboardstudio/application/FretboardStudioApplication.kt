package com.fretboardstudio.application

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan(basePackages = arrayOf("com.fretboardstudio.web"))
open class FretboardStudioApplication

fun main(args: Array<String>) {
	SpringApplication.run(FretboardStudioApplication::class.java, *args)
}
