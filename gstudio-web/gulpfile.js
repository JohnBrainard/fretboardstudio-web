var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

gulp.task('less', function() {
	return gulp.src('./src/main/web/less/bootstrap.less')
		.pipe(less({
			paths: [ path.join(__dirname, 'less', 'includes') ]
		}))
		.pipe(gulp.dest('./build/resources/main/static/ui/css'))
});

gulp.task('build', ['less']);
