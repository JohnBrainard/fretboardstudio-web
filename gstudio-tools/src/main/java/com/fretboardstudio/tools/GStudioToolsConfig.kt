package com.fretboardstudio.tools

import com.fretboardstudio.dao.ChordDAO
import com.fretboardstudio.dao.ChordFingeringDAO
import com.fretboardstudio.dao.ScaleDAO
import com.fretboardstudio.dao.config.DaoConfig
import com.mysql.jdbc.Driver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.core.env.Environment
import org.springframework.core.env.PropertyResolver
import org.springframework.jdbc.datasource.DriverManagerDataSource
import javax.sql.DataSource

operator fun PropertyResolver.get(key: String): String? = getProperty(key)

@Configuration
@Import(DaoConfig::class)
open class GStudioToolsConfig {
	@Bean
	open fun dataSource(env: Environment): DataSource {
		val url = env["db.url"]
		val user = env["db.username"]
		val password = env["db.password"]

		println("Connection to $url as $user")

		val ds = DriverManagerDataSource(url, user, password)
		ds.setDriverClassName(Driver::class.qualifiedName)

		return ds
	}

	@Bean
	open fun importerTool(scaleDAO: ScaleDAO, chordDAO: ChordDAO, fingeringDAO: ChordFingeringDAO) = ImporterTool(scaleDAO, chordDAO, fingeringDAO)
}