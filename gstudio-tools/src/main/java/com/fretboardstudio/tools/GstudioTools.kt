package com.fretboardstudio.tools

import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.core.env.SimpleCommandLinePropertySource
import org.springframework.core.io.support.ResourcePropertySource
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Created by John on 10/4/15.
 */
object GStudioTools {
	@JvmStatic
	fun main(args:Array<String>) {
		val context = AnnotationConfigApplicationContext()
		context.register(GStudioToolsConfig::class.java)

		if (Files.exists(Paths.get("application.properties"))) {
			println("Found application properties in current directory...")
			context.environment.propertySources.addFirst(ResourcePropertySource(Paths.get("application.properties").toUri().toString()))
		}

		context.environment.propertySources.addLast(ResourcePropertySource("classpath:application.properties"))
		context.environment.propertySources.addLast(SimpleCommandLinePropertySource(*args))
		context.refresh()

		val importerTool = context.getBean(ImporterTool::class.java)

		importerTool.importChords()
		importerTool.importScales()
		importerTool.importFingerings()
	}
}