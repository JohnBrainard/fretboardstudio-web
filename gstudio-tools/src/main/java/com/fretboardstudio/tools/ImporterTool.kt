package com.fretboardstudio.tools

import com.fretboardstudio.dao.ChordDAO
import com.fretboardstudio.dao.ChordFingeringDAO
import com.fretboardstudio.dao.ScaleDAO
import com.fretboardstudio.loader.ChordFileImporter
import com.fretboardstudio.loader.ChordFingeringFileImporter
import com.fretboardstudio.loader.ScaleFileImporter
import com.fretboardstudio.util.ChordMapUtil
import org.springframework.stereotype.Component

/**
 * Created by John on 10/6/15.
 */

@Component
class ImporterTool (val scaleDAO:ScaleDAO, val chordDAO:ChordDAO, val fingeringDAO:ChordFingeringDAO) {
	fun importChords() {
		val importer = ChordFileImporter()
		val chords = importer.importFromDefaultResource()

		chords.forEach {
			println("Saving chord: ${it}")
			chordDAO.save(it)
		}

	}

	fun importScales() {
		val importer = ScaleFileImporter()
		val scales = importer.importFromDefaultResource()

		scales.forEach {
			println("Saving scale: ${it}")
			scaleDAO.save(it)
		}
	}

	fun importFingerings() {
		val chords = chordDAO.findAll()
		val chordMap = ChordMapUtil(chords)
		val importer = ChordFingeringFileImporter(chordMap)
		val fingerings = importer.importFromDefaultResource()

		fingerings.forEach {
			println("Saving fingering: ${it}")
			fingeringDAO.save(it)
		}
	}
}