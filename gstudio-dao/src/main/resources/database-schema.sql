drop table if exists chords;
create table chords (
	chord_id char(36) primary key,
    name varchar(100) not null,
    slug varchar(100) not null
);

drop table if exists chord_intervals;
create table chord_intervals (
	chord_id char(36) not null,
    chord_interval int not null,
    primary key(chord_id, chord_interval)
);

drop table if exists scales;
create table scales (
	scale_id char(36) primary key,
    name varchar(100) not null,
    slug varchar(100) not null
);

drop table if exists scale_intervals;
create table scale_intervals (
	scale_id char(36) not null,
    scale_interval int not null,
    primary key(scale_id, scale_interval)
);

drop table if exists chord_fingerings;
create table chord_fingerings (
	chord_fingering_id char(36) primary key,
    name varchar(100) not null,
	root_note int not null,
    chord_id char(36),
    fret int not null
);

drop table if exists chord_fingering_notes;
create table chord_fingering_notes (
	chord_fingering_id char(36) not null,
    note int not null,
    string int not null,
    fret int not null,
    finger int not null,
    optional bit not null,
    rootNote bit not null
);
