package com.fretboardstudio.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.fretboardstudio.ChordFingering;
import com.fretboardstudio.Scale;

public class ScaleFileImporter {
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(ScaleFileImporter.class
			.getName());

	private static final String SCALE_RESOURCE = "META-INF/scales.xml";

	/**
	 * Loads scales from an XML file and returns them in a
	 * <code>LinkedList</code>.
	 * 
	 * @param file
	 *            Reference to the XML file.
	 * @return List of scales in the order they were read from the XML.
	 * @throws LoaderException
	 */
	public List<Scale> importFromFile(File file) throws LoaderException {
		try {
			return importFromStream(new FileInputStream(file));
		} catch (Exception ex) {
			throw new LoaderException("Unable to load scales from file: "
					+ file.getAbsolutePath(), ex);
		}
	}

	/**
	 * Loads scales from an XML stream and returns them in a
	 * <code>LinkedList</code>.
	 * 
	 * @param stream
	 * @return List of scales in the order they were read from the XML.
	 * @throws LoaderException
	 */
	public List<Scale> importFromStream(InputStream stream)
			throws LoaderException {
		LinkedList<Scale> scales = new LinkedList<Scale>();

		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			Document doc = db.parse(stream);

			NodeList nodes = doc.getElementsByTagName("scale");
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				Scale scale = readNode((Element) node);

				scales.add(scale);
			}
		} catch (Exception ex) {
			throw new LoaderException("Unable to load scales from XML", ex);
		}

		return scales;
	}

	/**
	 * Import the chord data from META-INF/chords.xml
	 * 
	 * @return
	 * @throws IOException
	 */
	public List<Scale> importFromDefaultResource() throws LoaderException {
		URL url = this.getClass().getClassLoader().getResource(SCALE_RESOURCE);

		try {
			return importFromStream(url.openStream());
		} catch (IOException ex) {
			throw new LoaderException("Unable to load scales from "
					+ SCALE_RESOURCE, ex);
		}
	}

	Scale readNode(Element node) {
		Scale.Builder scale = new Scale.Builder();
		ArrayList<Integer> intervals = new ArrayList<Integer>();

		String name = node.getElementsByTagName("name").item(0)
				.getTextContent();

		scale.name(name);

		NodeList nodes = node.getElementsByTagName("interval");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element intervalN = (Element) nodes.item(i);

			Integer interval = Integer.valueOf(intervalN.getTextContent());
			scale.interval(interval);
		}

		return scale.build();
	}
}
