package com.fretboardstudio.loader;

public class LoaderException extends Exception {
	private static final long serialVersionUID = 8695160730007474100L;

	public LoaderException(String message) {
		super(message);
	}

	public LoaderException(String message, Throwable cause) {
		super(message, cause);
	}
}
