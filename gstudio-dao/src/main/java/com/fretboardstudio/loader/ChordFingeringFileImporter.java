package com.fretboardstudio.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import com.fretboardstudio.Chord;
import com.fretboardstudio.ChordFingering;
import com.fretboardstudio.ChordNote;
import com.fretboardstudio.Note;
import com.fretboardstudio.util.ChordMapUtil;
import com.fretboardstudio.util.FretboardStudioUtil;

/**
 * Utility class for importing raw chord data form a text file. 
 * 
 * @author john
 *
 */
public class ChordFingeringFileImporter {
	private final Logger log = Logger.getLogger(ChordFingeringFileImporter.class.getName());

	public static String CHORD_RESOURCE = "META-INF/rawchords.txt";

	private ChordMapUtil chordMap = null;

	public ChordFingeringFileImporter() {
		// TODO: What should go here?
	}
	
	/**
	 * If a <code>ChordMapUtil</code> object is present, lookup will be
	 * performed to determine the chord the fingering belongs to.
	 * 
	 * @param chordMap
	 */
	public ChordFingeringFileImporter(ChordMapUtil chordMap) {
		this.chordMap = chordMap;
	}

	/**
	 * Import the chord data from the specified file.
	 * 
	 * @param file
	 * @throws IOException
	 */
	public List<ChordFingering> importFromFile(File file) throws IOException {
		return importFromStream(new FileInputStream(file));
	}

	/**
	 * Import the chord data from META-INF/chords.xml
	 * 
	 * @return
	 * @throws IOException
	 */
	public List<ChordFingering> importFromDefaultResource() throws IOException {
		URL url = this.getClass().getClassLoader().getResource(CHORD_RESOURCE);
		
		return importFromStream(url.openStream());
	}

	/**
	 * Import the chord data from the specified stream.
	 * 
	 * @param stream
	 * @throws IOException
	 */
	public List<ChordFingering> importFromStream(InputStream stream) throws IOException {
		ArrayList<ChordFingering> chords = new ArrayList<ChordFingering>();
		BufferedReader in = new BufferedReader(new InputStreamReader(stream));

		// For logging purposes, count the # of fingerings without associated
		// chords.
		int count = 0;
		String line = in.readLine();
		while(line != null) {
			ChordFingering chord = readChord(line);

			log.finest("Read chord: " + chord.toString());
			chords.add(chord);

//			if(this.chordMap != null && chord.getChordKey() == null)
			if (this.chordMap != null && chord.getChordId() != null)
				count++;

			line = in.readLine();
		}

		log.info("Total Chord Fingerings: " + chords.size());
		log.info("Fingerings w/out chord references: " + count);

		return chords;
	}

	/**
	 * Read a chord from the raw chord data and create a new chord
	 * object.
	 * 
	 * @param spec
	 * @return
	 */
	public ChordFingering readChord(String spec) {
		log.finest("Reading chord spec: " + spec);
		ChordFingering.Builder builder = new ChordFingering.Builder();
		String[] parts = StringUtils.split(spec, '|');

		char[]	frets = parts[0].toCharArray();
		char[]	fingers = parts[1].toCharArray();
		// TODO: Add use for optional notes.
		char[] 	optional = parts[2].toCharArray();
		int 	startFret = Integer.parseInt(parts[3]);
		String 	name = parts[4];

		Set<Note> notes = new TreeSet<Note>();

		for(int idx = 0; idx < frets.length; idx++) {
			int specFret = toIntValue(frets[idx]);
			int fret = specFret == -1 ? -1 : startFret + specFret - 1;
			int finger = toIntValue(fingers[idx]);

			Note note = 
					specFret == -1 ? Note.MUTED : 
					FretboardStudioUtil.getNoteAtFretOnString(idx, fret);

			// Don't add muted strings (X).
			if(note != Note.MUTED)
				notes.add(note);

			boolean rootNote = note.equals(getNoteFromChordName(name));

			ChordNote.Builder noteBuilder = new ChordNote.Builder();
			noteBuilder.fret(fret);
			noteBuilder.finger(finger);
			noteBuilder.isRootNote(rootNote);
			noteBuilder.string(idx);
			noteBuilder.note(note);

			builder.note(noteBuilder.build());
		}

		String rootNote = getNoteFromChordName(name);

		builder.name(name);
		builder.rootNote(Note.getNote(rootNote));
		builder.fret(startFret);

		// Look up the real chord this fingering belongs to.
		if(chordMap != null) {
			int[] intervals = FretboardStudioUtil.convertNotesToIntervals(builder.getRootNote(), notes.toArray(new Note[]{}));
			Chord realChord = chordMap.findChord(intervals);

			if(realChord != null) {
				builder.chordId(realChord.getId());
			} else {
				log.info("Unable to find real chord for fingering: " + builder.getName());
			}
		}

		return builder.build();
	}

	int toIntValue(char val) {
		return (val == 'x' || val == '.') ? -1 : CharUtils.toIntValue(val);
	}

	String getNoteFromChordName(String name) {
		char[] parts = name.toCharArray();
		String noteName = String.valueOf(parts[0]);

		if(parts.length > 1) {
			if(parts[1] == '#' || parts[1] == 'b')
				noteName += parts[1];
		}

		return noteName;
	}
}