package com.fretboardstudio.loader;

import com.fretboardstudio.Chord;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class ChordFileImporter {
	private static final Logger log = Logger.getLogger(ChordFileImporter.class.getName());
	private static final String CHORD_RESOURCE = "META-INF/chords.xml";

	/**
	 * Loads scales from an XML file and returns them in a <code>LinkedList</code>.
	 *
	 * @param file Reference to the XML file.
	 * @return List of scales in the order they were read from the XML.
	 * @throws LoaderException
	 */
	public List<Chord> importFromFile(File file) throws LoaderException {
		try {
			return importFromStream(new FileInputStream(file));
		} catch (Exception ex) {
			throw new LoaderException("Unable to load chords from file: " + file.getAbsolutePath(), ex);
		}
	}

	/**
	 * Loads scales from an XML stream and returns them in a <code>LinkedList</code>.
	 *
	 * @param stream
	 * @return List of scales in the order they were read from the XML.
	 * @throws LoaderException
	 */
	public List<Chord> importFromStream(InputStream stream) throws LoaderException {
		LinkedList<Chord> chords = new LinkedList<Chord>();

		try {
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = db.parse(stream);

			NodeList nodes = doc.getElementsByTagName("chord");
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				Chord chord = readNode((Element) node);

				chords.add(chord);
			}
		} catch (Exception ex) {
			throw new LoaderException("Unable to load scales from XML", ex);
		}

		return chords;
	}

	/**
	 * Import the chord data from META-INF/chords.xml
	 *
	 * @return
	 * @throws IOException
	 */
	public List<Chord> importFromDefaultResource() throws LoaderException {
		URL url = this.getClass().getClassLoader().getResource(CHORD_RESOURCE);

		try {
			return importFromStream(url.openStream());
		} catch (IOException ex) {
			throw new LoaderException("Unable to load scales from " + CHORD_RESOURCE, ex);
		}
	}


	Chord readNode(Element node) {
		Chord.Builder builder = new Chord.Builder();

		String name = node.getElementsByTagName("name").item(0).getTextContent();

		NodeList nodes = node.getElementsByTagName("interval");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element intervalN = (Element) nodes.item(i);

			Integer interval = Integer.valueOf(intervalN.getTextContent());

			builder.interval(interval);
		}

		builder.name(name);

		return builder.build();
	}
}
