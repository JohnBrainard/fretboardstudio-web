package com.fretboardstudio.dao;

import com.fretboardstudio.Scale;
import com.fretboardstudio.ScaleChords;

import java.util.List;

public interface ScaleChordsDAO extends DAO<ScaleChords> {
//	public List<ScaleChords> findForNoteAndScale(Note note, Scale scale) {
//		Objectify ofy = ObjectifyService.begin();
//		
//		Query<ScaleChords> q = ofy.query(ScaleChords.class)
//				.filter("note", note)
//				.filter("scale", scale);
//
//		return q.list();
//	}
	
	List<ScaleChords> findForScale(Scale scale);

	// TODO: move this out to a non-DAO class
	/*
	Set<Key<Chord>> findChordKeysForScale(Scale scale) {
		List<ScaleChords> scaleChords = findForScale(scale);
		Set<Key<Chord>> keys = new TreeSet<Key<Chord>>();

		for(ScaleChords chords : scaleChords) {
			keys.addAll(chords.getChords());
		}

		return keys;
	}
	*/
}
