package com.fretboardstudio.dao;

import java.io.Serializable;
import java.util.List;

/**
 * Created by John on 10/3/15.
 */
public interface DAO<T extends Object> extends Serializable {
	T get(String id);

	List<T> findAll();

	List<T> findAll(String sort);

	/**
	 * Save entity and return it's record id
	 */
	String save(T record);

	void save(T... records);

	void save(Iterable<T> records);

	void delete(String id);
}
