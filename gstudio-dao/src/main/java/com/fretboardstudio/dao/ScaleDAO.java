package com.fretboardstudio.dao;

import com.fretboardstudio.Scale;

import java.util.List;

public interface ScaleDAO extends DAO<Scale> {
	String MC_FIND_ALL_KEY = ScaleDAO.class.getName() + "_findAll";
	
	Scale findByName(String name);

	List<Scale> findAll(); /* {
		MemcacheService mcsvc = MemcacheServiceFactory.getMemcacheService();
		@SuppressWarnings("unchecked")
		List<Scale> scales = (List<Scale>) mcsvc.get(MC_FIND_ALL_KEY);

		if(scales == null || scales.size() == 0) {
			scales = super.findAll();

			log.info("Storing scales in memcache.");
			mcsvc.put(MC_FIND_ALL_KEY, scales);
		}

		return scales;
	} */
}
