package com.fretboardstudio.dao;

import com.fretboardstudio.Chord;
import com.fretboardstudio.ChordFingering;
import com.fretboardstudio.Note;

import java.util.List;

public interface ChordFingeringDAO extends DAO<ChordFingering> {
	String[] CHROMATIC_SCALE = new String[]{
			"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
	};

	ChordFingering findById(long id);

	List<ChordFingering> findAll();

	List<ChordFingering> findAllForNote(Note note);

	List<ChordFingering> findAllForChord(Note root, Chord chord);

	List<ChordFingering> findAllForChords(Note root, List<String> chords);

	List<ChordFingering> findFingeringsWithoutChords();
}