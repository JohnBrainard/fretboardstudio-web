package com.fretboardstudio.dao.mysql

import com.fretboardstudio.Chord
import com.fretboardstudio.ChordFingering
import com.fretboardstudio.Note
import com.fretboardstudio.dao.ChordFingeringDAO
import com.fretboardstudio.util.forEach
import com.fretboardstudio.util.forEachGroup
import com.fretboardstudio.util.orDefault
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations
import java.util.*

/**
 * Created by John on 10/7/15.
 */
class MySQLChordFingeringDAO(@Autowired val jdbc: JdbcOperations, @Autowired val namedJdbcTemplate: NamedParameterJdbcOperations) : ChordFingeringDAO {

	private val mapper = RowMapper { rs, index ->
		ChordFingering.Builder()
				.id(rs.getString(1))
				.name(rs.getString(2))
				.rootNote(Note.getNote(rs.getInt(3)))
				.chordId(rs.getString(4))
				.fret(rs.getInt(5))
	}

	override fun get(id: String): ChordFingering? {
		val sql = """
			select
			    chord_fingering_id,
			    name,
			    root_note,
			    chord_id,
			    fret
			from chord_fingerings
			where chord_fingering_id=?
		"""

		val builder = jdbc.queryForObject(sql, mapper, id)
		loadFingerings(builder, id)
		return builder.build()
	}

	override fun findAll(sort: String?): MutableList<ChordFingering>? {
		throw UnsupportedOperationException()
	}

	override fun save(record: ChordFingering): String? {
		val id = record.id.orDefault(UUID.randomUUID().toString())

		if (record.id == null) {
			val sql = """
				insert into chord_fingerings (chord_fingering_id, name, root_note, chord_id, fret)
					values (?, ?, ?, ?, ?)
			"""

			jdbc.update(sql, id, record.name, record.rootNote?.position, record.chordId, record.fret)
			saveChordNotes(id, record)
		} else {
			throw UnsupportedOperationException("Only saving a new record is currently supported")
		}

		return id
	}

	override fun save(vararg records: ChordFingering?) {
		throw UnsupportedOperationException()
	}

	override fun save(records: MutableIterable<ChordFingering>?) {
		throw UnsupportedOperationException()
	}

	override fun delete(id: String?) {
		jdbc.update("delete from chord_fingerings where chord_fingering_id=?", id)
		jdbc.update("delete from chord_fingering_notes where chord_fingering_id=?", id)
	}

	override fun findById(id: Long): ChordFingering? {
		throw UnsupportedOperationException()
	}

	override fun findAll(): MutableList<ChordFingering>? {
		throw UnsupportedOperationException()
	}

	override fun findAllForNote(note: Note?): MutableList<ChordFingering>? {
		throw UnsupportedOperationException()
	}

	override fun findAllForChord(root: Note, chord: Chord): List<ChordFingering> {
		val sql = """
			select c.chord_fingering_id, c.name, c.root_note, c.chord_id, c.fret,
				n.note, n.string, n.fret, n.finger, n.optional, n.rootNote
			from chord_fingerings c
			left join chord_fingering_notes n on n.chord_fingering_id=c.chord_fingering_id
			where c.root_note=? and c.chord_id=?
		"""

		val fingerings = ArrayList<ChordFingering>()
		var builder: ChordFingering.Builder? = null

		jdbc.queryForRowSet(sql, root.position, chord.id).forEachGroup(1) { newGroup, rs ->
			if (newGroup) {
				if (builder != null)
					fingerings.add(builder!!.build())

				builder = ChordFingering.Builder()
						.id(rs.getString(1))
						.name(rs.getString(2))
						.rootNote(Note.getNote(rs.getInt(3)))
						.chordId(rs.getString(4))
						.fret(rs.getInt(5))
			}

			builder?.note {
				note(Note.getNote(rs.getInt(6)))
				string(rs.getInt(7))
				fret(rs.getInt(8))
				finger(rs.getInt(9))
				isOptional(rs.getBoolean(10))
				isRootNote(rs.getBoolean(11))
			}
		}

		if (builder != null)
			fingerings.add(builder!!.build())

		return fingerings
	}

	override fun findAllForChords(root: Note, chords: List<String>): List<ChordFingering> {
		val sql = """
			select c.chord_fingering_id, c.name, c.root_note, c.chord_id, c.fret,
				n.note, n.string, n.fret, n.finger, n.optional, n.rootNote
			from chord_fingerings c
			left join chord_fingering_notes n on n.chord_fingering_id=c.chord_fingering_id
			where c.root_note=:note and c.chord_id in (:chords)
		"""

		if (chords.isEmpty())
			return emptyList()

		val fingerings = ArrayList<ChordFingering>()
		var builder: ChordFingering.Builder? = null

		val params = HashMap<String, Any>()
		params["note"] = root.position
		params["chords"] = chords

		namedJdbcTemplate.queryForRowSet(sql, params).forEachGroup(1) { newGroup, rs ->
			if (newGroup) {
				if (builder != null)
					fingerings.add(builder!!.build())

				builder = ChordFingering.Builder()
						.id(rs.getString(1))
						.name(rs.getString(2))
						.rootNote(Note.getNote(rs.getInt(3)))
						.chordId(rs.getString(4))
						.fret(rs.getInt(5))
			}

			builder?.note {
				note(Note.getNote(rs.getInt(6)))
				string(rs.getInt(7))
				fret(rs.getInt(8))
				finger(rs.getInt(9))
				isOptional(rs.getBoolean(10))
				isRootNote(rs.getBoolean(11))
			}
		}

		if (builder != null)
			fingerings.add(builder!!.build())

		return fingerings
	}

	override fun findFingeringsWithoutChords(): MutableList<ChordFingering>? {
		throw UnsupportedOperationException()
	}

	private fun saveChordNotes(id: String, fingering: ChordFingering) {
		jdbc.update("delete from chord_fingering_notes where chord_fingering_id=?", id)
		val sql = """
			insert into chord_fingering_notes (
				chord_fingering_id, note, string, fret, finger, optional, rootNote)
				values (?, ?, ?, ?, ?, ?, ?)
		"""
		jdbc.batchUpdate(sql, fingering.notes.map {
			arrayOf<Any>(
					id, it.note.position, it.string, it.fret, it.finger, it.optional, it.rootNote
			)
		})
	}

	private fun loadFingerings(builder: ChordFingering.Builder, id: String) {
		val sql = """
			select
				chord_fingering_id,
				note,
				string,
				fret,
				finger,
				optional,
				rootNote
			from chord_fingering_notes
			where chord_fingering_id=?
		"""

		jdbc.queryForRowSet(sql, id).forEach { rs ->
			builder.note {
				note(Note.getNote(rs.getInt(2)))
				string(rs.getInt(3))
				fret(rs.getInt(4))
				finger(rs.getInt(5))
				isOptional(rs.getBoolean(6))
				isRootNote(rs.getBoolean(7))
			}
		}
	}
}