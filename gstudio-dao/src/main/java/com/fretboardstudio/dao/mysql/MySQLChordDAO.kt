package com.fretboardstudio.dao.mysql

import com.fretboardstudio.Chord
import com.fretboardstudio.dao.ChordDAO
import com.fretboardstudio.util.forEach
import com.fretboardstudio.util.forEachGroup
import com.fretboardstudio.util.orDefault
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import java.util.*

class MySQLChordDAO
@Autowired
constructor(private val jdbc: JdbcOperations) : ChordDAO {
	val chordMapper = RowMapper { rs, index ->
		Chord.Builder()
				.id(rs.getString(1))
				.name(rs.getString(2))
				.slug(rs.getString(3))
	}


	override fun findByName(name: String): Chord? {
		val sql = """
			select chord_id, name, slug from chords where name=?
		"""

		val chordBuilder = jdbc.queryForObject(sql, chordMapper, name)
		return loadIntervals(chordBuilder)
	}

	override fun get(id: String?): Chord? {
		val sql = """
			select chord_id, name, slug from chords where chord_id=?
		"""

		val builder = jdbc.queryForObject(sql, chordMapper, id)
		return loadIntervals(builder)
	}

	override fun findAll(): List<Chord>? {
		val sql = """
			select c.chord_id, c.name, i.chord_interval
			from chords c
			left join chord_intervals i on i.chord_id = c.chord_id
			order by c.name, c.chord_id
		"""

		val chords = ArrayList<Chord>()
		var builder: Chord.Builder? = null
		jdbc.queryForRowSet(sql).forEachGroup(1) { newGroup, rowSet ->
			if (newGroup) {
				if (builder != null)
					chords.add(builder!!.build())

				builder = Chord.Builder()
						.id(rowSet.getString(1))
						.name(rowSet.getString(2))
			}

			builder?.interval(rowSet.getInt(3))
		}

		if (builder != null)
			chords.add(builder!!.build())

		return chords
	}

	override fun findAll(sort: String): List<Chord>? {
		return null
	}

	override fun save(record: Chord): String {
		val id = record.id.orDefault(UUID.randomUUID().toString())

		if (record.id == null) {
			jdbc.update("insert into chords (chord_id, name, slug) values (?, ?, ?)",
					id,
					record.name,
					record.slug)
		} else {
			throw UnsupportedOperationException("Only saving a new record is currently supported")
		}

		jdbc.batchUpdate("insert into chord_intervals (chord_id, chord_interval) values (?, ?)",
				record.intervals.map { arrayOf<Any>(id, it) }.toList())

		return id
	}

	override fun save(vararg records: Chord) {
		records.forEach { save(it) }
	}

	override fun save(records: Iterable<Chord>) {
		records.forEach { save(it) }
	}

	override fun delete(id: String?) {
		jdbc.update("delete from chord_intervals where chord_id=?", id)
		jdbc.update("delete from chords where chord_id=?", id)
	}

	private fun loadIntervals(builder: Chord.Builder): Chord {
		val sql = """
			select chord_interval from chord_intervals
			where chord_id=?
			order by chord_interval
		"""

		jdbc.queryForRowSet(sql, builder.id).forEach {
			builder.interval(it.getInt(1))
		}

		return builder.build()
	}

	private fun loadIntervals(vararg builders: Chord.Builder): List<Chord> {
		val sql = """
			select interval from chord_intervals
			where chord_id in (?)
			order by chord_id, interval
		"""

		val chords = ArrayList<Chord>()
		builders.forEach {
			chords += loadIntervals(it)
		}
		return chords
	}

	companion object {
		private val serialVersionUID = -2283699959913011709L
	}
}

