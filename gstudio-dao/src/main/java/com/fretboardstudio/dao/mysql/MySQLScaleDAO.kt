package com.fretboardstudio.dao.mysql

import com.fretboardstudio.Chord
import com.fretboardstudio.Scale
import com.fretboardstudio.dao.ScaleDAO
import com.fretboardstudio.util.forEach
import com.fretboardstudio.util.forEachGroup
import com.fretboardstudio.util.orDefault
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.jdbc.core.RowMapper
import java.util.*

class MySQLScaleDAO(@Autowired val jdbc: JdbcOperations) : ScaleDAO {
	val scaleMapper = RowMapper { rs, index ->
		Scale.Builder()
				.id(rs.getString(1))
				.name(rs.getString(2))
				.slug(rs.getString(3))
	}

	override fun findByName(name: String): Scale? {
		val sql = """
			select scale_id, name, slug from scales where name=?
		"""

		val chordBuilder = jdbc.queryForObject(sql, scaleMapper, name)
		return loadIntervals(chordBuilder)
	}

	override fun get(id: String?): Scale? {
		val sql = """
			select scale_id, name, slug from scales where scale_id=?
		"""

		var scaleBuilder = jdbc.queryForObject(sql, scaleMapper, id);
		return loadIntervals(scaleBuilder)
	}

	override fun findAll(sort: String?): MutableList<Scale>? {
		throw UnsupportedOperationException()
	}

	override fun save(record: Scale): String? {
		val id = record.id.orDefault(UUID.randomUUID().toString())

		if (record.id == null) {
			jdbc.update("insert into scales (scale_id, name, slug) values (?, ?, ?)",
					id,
					record.name,
					record.slug)
		} else {
			throw UnsupportedOperationException("Only saving a new record is currently supported")
		}

		jdbc.batchUpdate("insert into scale_intervals (scale_id, scale_interval) values (?, ?)",
				record.intervals.map { arrayOf<Any>(id, it) }.toList())

		return id
	}

	override fun save(vararg records: Scale?) {
		throw UnsupportedOperationException()
	}

	override fun save(records: MutableIterable<Scale>?) {
		throw UnsupportedOperationException()
	}

	override fun delete(id: String?) {
		jdbc.update("delete from scale_intervals where scale_id=?", id)
		jdbc.update("delete from scales where scale_id=?", id)
	}

	override fun findAll(): List<Scale>? {
		val sql = """
			select s.scale_id, s.name, i.scale_interval
			from scales s
			left join scale_intervals i on i.scale_id=s.scale_id
			order by s.name, s.scale_id
		"""

		val scales = ArrayList<Scale>()
		var builder: Scale.Builder? = null

		jdbc.queryForRowSet(sql).forEachGroup(1) { newGroup, rowSet ->
			if (newGroup) {
				if (builder != null)
					scales.add(builder!!.build())

				builder = Scale.Builder()
						.id(rowSet.getString(1))
						.name(rowSet.getString(2))
			}

			builder?.interval(rowSet.getInt(3))
		}

		if (builder != null)
			scales.add(builder!!.build())

		return scales
	}

	private fun loadIntervals(builder: Scale.Builder): Scale {
		val sql = """
			select scale_interval from scale_intervals
			where scale_id=?
			order by scale_interval
		"""

		jdbc.queryForRowSet(sql, builder.id).forEach {
			builder.interval(it.getInt(1))
		}

		return builder.build()
	}

	private fun loadIntervals(vararg builders: Chord.Builder): List<Scale> {
		val scales = ArrayList<Scale>()
		builders.forEach {
			scales += loadIntervals(it)
		}
		return scales
	}
}