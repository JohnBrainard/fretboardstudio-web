package com.fretboardstudio.dao.config;

import com.fretboardstudio.dao.ChordDAO;
import com.fretboardstudio.dao.ChordFingeringDAO;
import com.fretboardstudio.dao.ScaleChordsDAO;
import com.fretboardstudio.dao.ScaleDAO;
import com.fretboardstudio.dao.mysql.MySQLChordDAO;
import com.fretboardstudio.dao.mysql.MySQLChordFingeringDAO;
import com.fretboardstudio.dao.mysql.MySQLScaleDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 * Created by John on 10/3/15.
 */

@Configuration
public class DaoConfig {

	@Bean
	JdbcOperations jdbcOperations(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	NamedParameterJdbcOperations namedParameterJdbcOperations(DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}

	@Bean
	ChordDAO chordDAO(JdbcOperations jdbc) {
		return new MySQLChordDAO(jdbc);
	}

	@Bean
	ChordFingeringDAO chordFingeringDAO(JdbcOperations jdbc, NamedParameterJdbcOperations namedJdbcOperations) {
		return new MySQLChordFingeringDAO(jdbc, namedJdbcOperations);
	}

	@Bean
	ScaleChordsDAO scaleChordsDAO(JdbcOperations jdbc) {
		return null;
	}

	@Bean
	ScaleDAO scaleDAO(JdbcOperations jdbc) {
		return new MySQLScaleDAO(jdbc);
	}

}