package com.fretboardstudio.dao;

import com.fretboardstudio.Chord;

/**
 * Created by John on 10/3/15.
 */
public interface ChordDAO extends DAO<Chord> {
	Chord findByName(String name);
}
