package com.fretboardstudio.util

import org.springframework.jdbc.support.rowset.SqlRowSet

fun SqlRowSet.forEachGroup(groupCol: Int, fn: (Boolean, SqlRowSet) -> Unit) {
	var group: Any? = null

	while (next()) {
		val newGroup = getObject(groupCol)
		fn(!newGroup.equals(group), this)
		group = newGroup
	}
}

fun SqlRowSet.forEach(fn:(SqlRowSet)->Unit) {
	while(next())
		fn(this)
}