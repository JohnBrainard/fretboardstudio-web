package com.fretboardstudio.dao;

import com.fretboardstudio.Chord;
import com.fretboardstudio.dao.mysql.MySQLChordDAO;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.logging.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = DaoTestConfig.class)
public class TestChordDAO extends TestCase {
	private static final Logger log = Logger.getLogger(TestChordDAO.class.getName());

	@Autowired
	MySQLChordDAO dao;

	@Test
	public void testFindAll() {
		List<Chord> chords = dao.findAll();
		assertFalse(chords.isEmpty());
	}

	@Test(expected = EmptyResultDataAccessException.class)
	public void testChordOperations() throws InterruptedException {
		for (Chord chord : CHORDS) {
			String id = dao.save(chord);
			assertNotNull(id);

			Chord savedChord = dao.get(id);
			assertNotNull(savedChord);
			assertTrue(savedChord.getIntervals().length != 0);
			assertEquals(id, savedChord.getId());

			dao.delete(id);
			dao.get(id);
		}
	}

//	@Test
//	public void testSaveMultiple() {
//		dao.save(CHORDS);
//
//		List<Chord> chords = dao.findAll();
//		for (Chord chord : chords) {
//			log.info("Chord: " + chord.toString());
//		}
//
//		assertEquals(CHORDS.length, chords.size());
//	}
//
//	@Test
//	public void testFindByName() throws Exception {
//		SetupUtils.setUpChordData(dao);
//
//		Chord chord = dao.findByName("Major");
//
//		assertNotNull("Expecting MySQLChordDAO.findByName('Major') to return a chord", chord);
//		assertEquals("Expecting returned chord to have name 'Major'", "Major", chord.getName());
//	}

	/*************************************************************************
	 * Constant helpers....
	 *************************************************************************/
	public static final Chord[] CHORDS = new Chord[]{
			new Chord.Builder()
					.name("Maj")
					.intervals(0, 4, 7)
					.build(),
			new Chord.Builder()
					.name("m")
					.intervals(0, 3, 7)
					.build(),
			new Chord.Builder()
					.name("maj7")
					.intervals(0, 3, 7, 11)
					.build()
	};

}
