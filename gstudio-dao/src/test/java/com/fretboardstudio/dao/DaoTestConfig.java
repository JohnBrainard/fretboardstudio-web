package com.fretboardstudio.dao;

import com.fretboardstudio.dao.config.DaoConfig;
import com.mysql.jdbc.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * Created by John on 10/3/15.
 */
@Import(DaoConfig.class)
@Configuration
public class DaoTestConfig {
	@Bean
	@Profile("dev")
	public DataSource devDataSource(Environment environment) {
		String url = environment.getProperty("192.168.1.118");
		String user = environment.getProperty("gstudio");
		String password = environment.getProperty("password");

		DriverManagerDataSource ds = new DriverManagerDataSource(url, user, password);
		ds.setDriverClassName(Driver.class.getName());

		return ds;
	}


	@Bean
	@Profile("test")
	public DataSource testDataSource(Environment environment) {
		String url = "jdbc:mysql://192.168.1.118:3306/gstudio";
		String user = "gstudio";
		String password = "password";

		DriverManagerDataSource ds = new DriverManagerDataSource(url, user, password);
		ds.setDriverClassName(Driver.class.getName());

		return ds;
	}

}
