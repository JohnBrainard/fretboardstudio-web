package com.fretboardstudio.dao;


import com.fretboardstudio.Scale;
import com.fretboardstudio.dao.mysql.MySQLScaleDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = DaoTestConfig.class)
public class TestScaleDAO {
	private static final Logger log = Logger.getLogger(TestScaleDAO.class.getName());

	@Autowired
	MySQLScaleDAO dao;

	@Test(expected = EmptyResultDataAccessException.class)
	public void testScaleOperations() {
		for (Scale scale : SCALES) {
			String id = dao.save(scale);
			assertNotNull(id);

			Scale savedSCale = dao.get(id);
			assertNotNull(savedSCale);
			assertTrue(savedSCale.getIntervals().length != 0);
			assertEquals(id, savedSCale.getId());

			dao.delete(id);
			dao.get(id);
		}
	}

	@Test
	public void testFindAll() throws Exception {
		List<Scale> scales = dao.findAll();

		assertNotNull(scales);
		assertFalse(scales.isEmpty());
	}

	/*************************************************************************
	 * Constant helpers....
	 *************************************************************************/
	public static final Scale[] SCALES = new Scale[]{
			new Scale.Builder()
					.name("Major")
					.intervals(0, 2, 4, 5, 7, 9, 11)
					.build(),
			new Scale.Builder()
					.name("Minor")
					.intervals(0, 2, 3, 5, 7, 8, 11)
					.build(),
			new Scale.Builder()
					.name("Minor Desc.")
					.intervals(0, 2, 3, 5, 7, 8, 10)
					.build()
	};
}
