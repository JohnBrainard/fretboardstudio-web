package com.fretboardstudio.dao

import com.fretboardstudio.ChordFingering
import com.fretboardstudio.ChordNote
import com.fretboardstudio.Note
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@ContextConfiguration(classes = arrayOf(DaoTestConfig::class))
@RunWith(SpringJUnit4ClassRunner::class)
@ActiveProfiles("test")
class TestChordFingeringDAO : TestCase() {

	@Autowired
	lateinit var dao: ChordFingeringDAO

	@Autowired
	lateinit var chordDAO: ChordDAO

	val testFingering = ChordFingering.build {
		name("C")
		rootNote(Note.C)
		chordId("non-existent-chord")
		fret(1)
		note(ChordNote(Note.MUTED, 0, -1, -1, false, false))
		note(ChordNote(Note.C, 1, 3, 3, true, false))
		note(ChordNote(Note.E, 2, 2, 2, false, false))
		note(ChordNote(Note.G, 3, 0, -1, false, false))
		note(ChordNote(Note.C, 4, 1, 1, true, false))
		note(ChordNote(Note.E, 5, 0, -1, false, false))
	}

	@Test(expected = EmptyResultDataAccessException::class)
	fun testOperations() {
		val id = dao.save(testFingering)

		val fingering = dao.get(id)
		assertNotNull(fingering)
		assertTrue(fingering.notes.size() != 0)
		assertEquals(id, fingering.id)

		dao.delete(id)
		dao.get(id)
	}

	@Test
	fun testFindAllForChord() {
		val chord = chordDAO.findByName("Major")
		assertEquals("Major", chord.name)

		val fingerings = dao.findAllForChord(Note.C, chord);
		assertFalse(fingerings.isEmpty())
	}

	@Test
	fun testFindAllForChords() {
		val chords = arrayOf(
				chordDAO.findByName("Major"),
				chordDAO.findByName("m")
		)

		val fingerings = dao.findAllForChords(Note.C, chords.map { it.id })
		assertFalse(fingerings.isEmpty())
	}

	//	@Test
	//	public void testFindAllForChords() throws Exception {
	//		ChordFingeringDAO chordFingeringDAO = new ChordFingeringDAO();
	//		MySQLChordDAO chordDAO = new MySQLChordDAO();
	//		ScaleDAO scaleDAO = new ScaleDAO();
	//
	//		SetupUtils.setUpChordData(chordDAO);
	//		SetupUtils.setUpScaleData(scaleDAO);
	//		SetupUtils.setUpChordFingeringData(chordFingeringDAO, chordDAO);
	//
	//		List<Chord> chords = chordDAO.findAll();
	//		List<Scale> scales = scaleDAO.findAll();
	//
	//		ChordScaleMapUtil csMapUtil = new ChordScaleMapUtil(scales, chords);
	//		List<ScaleChords> scaleChords = csMapUtil.findChords(scales.get(0));
	//
	//		Note rootNote = Note.C;
	//
	//		for (ScaleChords sc : scaleChords) {
	//			Note note = rootNote.getNoteAt(sc.getInterval());
	//			log.info("Looking up chords for interval " + sc.getInterval() + " ( " + note.getName() + " ).");
	//
	//			List<ChordFingering> fingerings = chordFingeringDAO.findAllForChords(note, sc.getChords());
	//
	//			assertNotNull("Expected list of chord fingerings.", fingerings);
	//			// This test is invalid as there don't seem to be any diminished chords in the data...
	////			assertTrue("Expected list size to be larger than 0.", fingerings.size() > 0);
	//
	//			log.info("Found " + fingerings.size() + " chord fingerings.");
	//		}
	//	}


	//	@Test
	//	public void testSaveSingle() {
	//		ChordFingering chord = ChordFingering.withBuilder()
	//				.name("C")
	//				.notes(new Note[]{C, E, G})
	//				.rootNote(C)
	//				.chordNote(new ChordNote(MUTED, 0, -1, -1))
	//				.chordNote(new ChordNote(C, 1, 3, 3, true))
	//				.chordNote(new ChordNote(E, 2, 2, 2))
	//				.chordNote(new ChordNote(G, 3, 0, -1))
	//				.chordNote(new ChordNote(C, 4, 1, 1, true))
	//				.chordNote(new ChordNote(E, 5, 0, -1))
	//				.build();
	//
	//		ChordFingeringDAO dao = new ChordFingeringDAO();
	//
	//		long key = dao.save(chord);
	//
	//		ChordFingering stored = dao.findById(key);
	//
	//		assertEquals("C", stored.getName());
	//		assertArrayEquals(new Note[]{C, E, G}, stored.getNotes());
	//		assertEquals(6, stored.getChordNotes().length);
	//	}

	//	@Test
	//	public void testSaveMultiple() {
	//		ChordFingeringDAO dao = new ChordFingeringDAO();
	//
	//		dao.save(ChordFingeringTestUtil.CHORD_SAMPLES);
	//
	//		List<ChordFingering> chords = dao.findAll();
	//
	//		for (ChordFingering chord : chords) {
	//			log.info(chord.toString());
	//		}
	//	}

	//	@Test
	//	public void testFindFingeringsWithoutChords() throws Exception {
	//		ChordFingeringDAO chordFingeringDAO = new ChordFingeringDAO();
	//		MySQLChordDAO chordDAO = new MySQLChordDAO();
	//
	//		SetupUtils.setUpChordFingeringData(chordFingeringDAO, chordDAO);
	//
	//		List<ChordFingering> fingerings = chordFingeringDAO.findFingeringsWithoutChords();
	//
	//		log.info("Found fingerings without chords: " + fingerings.size());
	//
	//		assertNotNull(fingerings);
	//		assertTrue(fingerings.size() > 0);
	//	}

	//	@Test
	//	public void testFindAllForChord() throws Exception {
	//		ChordFingeringDAO chordFingeringDAO = new ChordFingeringDAO();
	//		MySQLChordDAO chordDAO = new MySQLChordDAO();
	//
	//		SetupUtils.setUpChordFingeringData(chordFingeringDAO, chordDAO);
	//
	//		Chord chord = chordDAO.findByName("Major");
	//
	//		assertNotNull("Expecting a chord instance.", chord);
	//		assertEquals("Expecting chord with name 'Major'", "Major", chord.getName());
	//
	//		List<ChordFingering> fingerings = chordFingeringDAO.findAllForChord(Note.C, chord);
	//
	//		log.info("MySQLChordDAO.findAllForChord returned " + fingerings.size() + " results");
	//		assertNotNull("Expecting a list of chord fingerings.", fingerings);
	//		assertTrue("Expecting size of fingerings list to be greater than 0", fingerings.size() > 0);
	//	}


	//	@Test
	//	public void testFindAllForChordsWithEmptyChords() {
	//		ChordFingeringDAO dao = new ChordFingeringDAO();
	//
	//		List<ChordFingering> fingerings = dao.findAllForChords(C, new ArrayList<Key<Chord>>());
	//
	//		assertNotNull(fingerings);
	//		assertEquals(0, fingerings.size());
	//	}

	//	@Test
	//	public void testFindAllForNote() throws Exception {
	//		ChordFingeringDAO dao = new ChordFingeringDAO();
	//		SetupUtils.setUpChordFingeringData(dao, new MySQLChordDAO());
	//
	//		List<ChordFingering> fingerings = dao.findAllForNote(Note.C);
	//
	//		assertNotNull("Expecting a non-null list of fingerings", fingerings);
	//		assertTrue("Expecting a non-empty list of chord fingerings for " + Note.C, fingerings.size() > 0);
	//
	//		log.info("findAllForNote returned " + fingerings.size() + " results");
	//	}
}
