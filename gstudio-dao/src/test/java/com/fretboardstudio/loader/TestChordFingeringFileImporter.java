package com.fretboardstudio.loader;

import com.fretboardstudio.ChordFingering;
import com.fretboardstudio.Note;
import com.fretboardstudio.dao.ChordDAO;
import com.fretboardstudio.dao.ChordFingeringDAO;
import com.fretboardstudio.util.ChordMapUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import static com.fretboardstudio.Note.*;
import static org.junit.Assert.*;

public class TestChordFingeringFileImporter {
	private final String[] chordSpecs = new String[] {
			"x32010|.32.1.|3.....|1|C",
			"4111xx|4111..|......|5|Cadd9(no3) (5 fr.)",
			"xx4332|..4231|......|1|Daug (1fr.)2",
			"131113|131114|......|1|Fm9",
			"4321xx|4321..|......|1|Abaug (1fr.)2"
	};

	@Autowired
	ChordFingeringDAO fingeringDAO;

	@Autowired
	ChordDAO chordDAO;

	@Test
	public void testNoteFromChordName() {
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter();

		String[] chordNames = new String[] {
				"C", "C#", "Cm7", "C7sus", "Cb", "Cbsus", "C#sus"
		};
		
		String[] expected = new String[] {
				"C", "C#", "C", "C", "Cb", "Cb", "C#"
		};
		
		for(int i = 0; i < chordNames.length; i++) {
			String chordName = importer.getNoteFromChordName(chordNames[i]);

			assertEquals(expected[i], chordName);
		}
	}

	@Test
	public void testReadChord() {
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter();
		String spec = "x32010|.32.1.|3.....|1|C";

		ChordFingering chord = importer.readChord(spec);

		assertEquals("C", chord.getName());
		assertEquals(Note.C, chord.getRootNote());
		assertEquals("Expecing fret=1", 1, chord.getFret());
		assertArrayEquals(new Note[] { C, E, G }, chord.getRawNotes());
	}

	@Test
	public void testReadChords2() {
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter();
		String spec = "x2121x|.2131.|......|8|F#7-9 (8 fr.)";

		ChordFingering chord = importer.readChord(spec);

		assertEquals("F#7-9 (8 fr.)", chord.getName());
		assertEquals(F_SHARP, chord.getRootNote());
		assertEquals("Expecting fret=8", 8, chord.getFret());
		assertArrayEquals(new Note[] { A_SHARP, E, F_SHARP, G }, chord.getRawNotes());
	}
	
	@Test
	public void testReadChords3() {
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter();
		String spec = "x3455x|.1234.|......|1|C-5";
		
		ChordFingering chord = importer.readChord(spec);
		
		assertEquals("C-5", chord.getName());
		assertEquals(C, chord.getRootNote());
		assertEquals(1, chord.getFret());
	}

	@Test
	public void testReadChordFile() throws IOException {
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter();
		URL rawchords = ChordFingeringFileImporter.class.getClassLoader().getResource("META-INF/rawchords.txt");

		assertNotNull(rawchords);

		List<ChordFingering> chords = importer.importFromStream(rawchords.openStream());

		assertEquals(1221, chords.size());

		fingeringDAO.save(chords);
	}

	@Test
	public void testReadChordsWithChordMap() throws Exception {

		SetupUtils.setUpChordData(chordDAO);
		ChordMapUtil chordMap = new ChordMapUtil(chordDAO.findAll());

		ChordFingeringFileImporter importer = new ChordFingeringFileImporter(chordMap);
		List<ChordFingering> fingerings = importer.importFromDefaultResource();

		fingeringDAO.save(fingerings);
	}
}
