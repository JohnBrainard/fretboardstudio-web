package com.fretboardstudio.loader;

import java.net.URL;
import java.util.List;

import com.fretboardstudio.dao.ChordDAO;
import org.junit.Ignore;

import com.fretboardstudio.Chord;
import com.fretboardstudio.ChordFingering;
import com.fretboardstudio.Scale;
import com.fretboardstudio.dao.mysql.MySQLChordDAO;
import com.fretboardstudio.dao.ChordFingeringDAO;
import com.fretboardstudio.dao.ScaleDAO;
import com.fretboardstudio.util.ChordMapUtil;

@Ignore
public class SetupUtils {

	/**
	 * Loads the chord data from the META-INF/chords.xml file and stores them
	 * in the database for use with other tests.
	 * 
	 * @param dao
	 * @throws Exception
	 */
	public static void setUpChordData(ChordDAO dao) throws Exception {
		ChordFileImporter importer = new ChordFileImporter();
		URL url = SetupUtils.class.getClassLoader().getResource("META-INF/chords.xml");
		List<Chord> chords = importer.importFromStream(url.openStream());

		dao.save(chords);
	}

	public static void setUpScaleData(ScaleDAO dao) throws Exception {
		ScaleFileImporter importer = new ScaleFileImporter();
		List<Scale> scales = importer.importFromDefaultResource();
		
		dao.save(scales);
	}
	
	public static void setUpChordFingeringData(ChordFingeringDAO dao, MySQLChordDAO chordDAO) throws Exception
	{
		setUpChordData(chordDAO);

		List<Chord> chords = chordDAO.findAll();
		ChordMapUtil chordMap = new ChordMapUtil(chords);
		ChordFingeringFileImporter importer = new ChordFingeringFileImporter(chordMap);
		List<ChordFingering> chordFingerings = importer.importFromDefaultResource();

		dao.save(chordFingerings);
	}
}
