package com.fretboardstudio.loader;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.List;

import org.junit.Test;

import com.fretboardstudio.Scale;

public class TestScaleFileImporter {
	@Test
	public void testScaleFileImporter() throws Exception {
		ScaleFileImporter importer = new ScaleFileImporter();
		URL scalesxml = getClass().getResource("/META-INF/scales.xml");

		assertNotNull(scalesxml);

		List<Scale> scales = importer.importFromStream(scalesxml.openStream());

		assertTrue(scales.size() > 0);

		for(Scale scale : scales) {
			System.out.println("Scale: " + scale.toString());
		}
	}
}
