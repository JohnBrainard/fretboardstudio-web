package com.fretboardstudio;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class TestTest {
	
	@Test
	public void testEmptyArray() {
		int[] array = new int[] { };
		List<Integer> list =  new ArrayList<Integer>();

		processArray(array);

	}
	
	
	public void processArray(int[] array) {
		for(int i = 0; i < array.length; i++) {
			System.out.println("Found item:  " + i);

			throw new RuntimeException("This block should never be executded.");
		}

		for(int item : array) {
			System.out.println("Found item:  " + item);

			throw new RuntimeException("This block should never be executded.");
		}
	}
}
